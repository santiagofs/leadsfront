/* globals angular */
angular.module('app.services')
.factory('UserService', ['$rootScope', function ($rootScope) {
    var user = null;

    return {
      set: function(newUser) {
        newUser.role = parseInt(newUser.role_id) === 2 ? 'admin' : 'agent';
        localStorage.setItem('user', JSON.stringify(newUser));
        user = newUser;

        $rootScope.$broadcast('user:login', user);
        return user;
      },
      current: function(){
        return user;
      },
      getStoredUser: function(){
        var storedUser = localStorage.getItem('user');
        if(storedUser) {
          user = JSON.parse(storedUser);
          return user;
        } else {
          return null;
        }
      },
      isLogged: function(){
        return !!user;
      },
      logout: function(){
		localStorage.removeItem('user');
		user = null;
		$rootScope.$broadcast('user:login', user);
        return;
      }
    };

}]);
