/* globals angular */
angular.module('app.http-services')
.factory('ApiService', ['$http', '$q', '$window', 'LEAD_CONFIG', 'UserService', '$state', function ($http, $q, $window, LEAD_CONFIG, UserService, $state) {

	var service = LEAD_CONFIG.SERVICE_URL;

	var createHttp = function(method, endpoint, data) {
		data = !!data ? data : {};
		if (UserService.isLogged()) {
			data.api_token = UserService.current().api_token;
		}

		var deferred = $q.defer();

		function success(res) {
			deferred.resolve(res.data);
		}

		function error(res) {
			if(res.status === 401 && endpoint!== 'login'){
				UserService.logout();
				$state.go('login');
			}
			deferred.reject(res.data);
		}


		if(method === 'get' || method === 'delete') {
			var tmp = {};
			angular.forEach(data, function(value, key){

				var newValue = value;
				if(key === 'filters' && Array.isArray(value)) {
					newValue =  JSON.stringify(value);
				}
				if( key === 'relations' && Array.isArray(value)) {
					newValue = value.join(',');
				}

				tmp[key] = newValue;
			})

			data = {params: tmp}
		}

		$http[method](service+endpoint, data).then(success, error);
		return deferred.promise;
	}


	return {
		get: createHttp,
		user: {
			login: function(data) {
				return createHttp('post', 'login', data);
			},
			get: function(data) {
				return createHttp('get', 'user', data);
			},
			edit: function(data) {
				return createHttp('post', 'user', data);
			},
			list: function(data) {
				return createHttp('post', 'users', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'users', data);
			},
			check: function() {
				return createHttp('get', 'users/check');
			}
		},
		campaign: {
			get: function(data) {
				return createHttp('get', 'campaign', data);
			},
			edit: function(data) {
				return createHttp('post', 'campaign', data);
			},
			list: function(data) {
				return createHttp('post', 'campaigns', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'campaigns', data);
			},
		},
		source: {
			get: function(data) {
				return createHttp('get', 'source', data);
			},
			edit: function(data) {
				return createHttp('post', 'source', data);
			},
			list: function(data) {
				return createHttp('post', 'sources', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'sources', data);
			},
		},
		role: {
			get: function(data) {
				return createHttp('get', 'user-role', data);
			},
			edit: function(data) {
				return createHttp('post', 'user-role', data);
			},
			list: function(data) {
				return createHttp('post', 'user-roles', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'user-roles', data);
			},
		},
		chat: {
			get: function(data) {
				return createHttp('get', 'chat', data);
			},
			edit: function(data) {
				return createHttp('post', 'chat', data);
			},
			list: function(data) {
				return createHttp('post', 'chats', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'chats', data);
			},
		},
		lead: {
			get: function(data) {
				return createHttp('get', 'leads/lead', data);
			},
			edit: function(data) {
				return createHttp('post', 'leads/lead', data);
			},
			list: function(data) {
				return createHttp('get', 'leads/leads', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'leads/lead', data);
			},
			import: function(data) {
				return createHttp('post', 'leads/import', data);
			},
			reassign: function(data) {
				return createHttp('post', 'leads/reassign', data);
			},
			massEdit: function(data) {
				return createHttp('post', 'leads/mass-edit', data);
			},
			auxs: function(data) {
				return createHttp('get', 'leads/auxs', data);
			},
			forAdmin: function(data) {
				return createHttp('get', 'leads/forAdmin', data);
			}
		},
		leadType: {
			get: function(data) {
				return createHttp('get', 'leads/type', data);
			},
			edit: function(data) {
				return createHttp('post', 'leads/type', data);
			},
			list: function(data) {
				return createHttp('post', 'leads/types', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'leads/types', data);
			},
		},
		leadStatus: {
			get: function(data) {
				return createHttp('get', 'leads/status', data);
			},
			edit: function(data) {
				return createHttp('post', 'leads/status', data);
			},
			list: function(data) {
				return createHttp('post', 'leads/statuses', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'leads/statuses', data);
			},
		},
		note: {
			get: function(data) {
				return createHttp('get', 'note', data);
			},
			edit: function(data) {
				return createHttp('post', 'note', data);
			},
			list: function(data) {
				return createHttp('post', 'notes', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'notes', data);
			},
		},
		notification: {
			unread: function(data) {
				return createHttp('get', 'notifications/unread', data);
			},
			read: function(data) {
				return createHttp('post', 'notification/read', data);
			},
			get: function(data) {
				return createHttp('get', 'notification', data);
			},
			edit: function(data) {
				return createHttp('post', 'notification', data);
			},
			list: function(data) {
				return createHttp('post', 'notifications', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'notifications', data);
			},
		},
		sale: {
			get: function(data) {
				return createHttp('get', 'sale', data);
			},
			edit: function(data) {
				return createHttp('post', 'sale', data);
			},
			list: function(data) {
				return createHttp('get', 'sales', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'sales', data);
			},
		},
		schedule: {
			get: function(data) {
				return createHttp('get', 'schedule', data);
			},
			edit: function(data) {
				return createHttp('post', 'schedule', data);
			},
			list: function(data) {
				return createHttp('post', 'schedules', data);
			},
			pending: function(data) {
				return createHttp('post', 'schedules/pending', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'schedules', data);
			},
		},
		timeline: {
			get: function(data) {
				return createHttp('get', 'timeline', data);
			},
			edit: function(data) {
				return createHttp('post', 'timeline', data);
			},
			list: function(data) {
				return createHttp('post', 'timelines', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'timelines', data);
			},
			calls: function(data) {
				return createHttp('get', 'pending-calls', data);
			}
		},
		event: {
			get: function(data) {
				return createHttp('get', 'timeline-event', data);
			},
			edit: function(data) {
				return createHttp('post', 'timeline-event', data);
			},
			list: function(data) {
				return createHttp('post', 'timeline-events', data);
			},
			'delete': function(data) {
				return createHttp('delete', 'timeline-events', data);
			},
		},
		reports: {
			all: function(data) {
				return createHttp('get', 'reports/all-sales', data);
			},
			agentTotals: function(data) {
				return createHttp('get', 'reports/agent-totals', data);
			},
			leads: function(data) {
				return createHttp('get', 'reports/leads', data);
			},
		},
		aux: function(data) {
			return createHttp('get', 'aux', data);
		}
	};

}]);
