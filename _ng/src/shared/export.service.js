/* globals angular */
angular.module('app.services')
	.factory('ExportService', ['$window', function ($window) {

		var sanitazeCell = function(cell) {
			var v = cell ? (angular.isString(cell) ? cell : cell.toString()) : '';
			return v ? v.replace(/\n/g, '.').replace(/,/g ,';') : '';
		}

		var itemToRow = function(item) {
			var row = [];

			if(Array.isArray(item)) {
				row = item.map(function(e,i,a) {
					return sanitazeCell(e);
				}).join(',');

			} else {
				row = Object.keys(item).map(function(key,i,a) {
				   return sanitazeCell(item[key]);
				}).join(',');
			}


			//console.log(row);
			return row;
		};

		var download = function(data, filename, target) {

			if(!target) target = '_self';

			var newWindow = $window.open("", target);
			var file = new Blob([data], { type: 'text/csv' });
            var fileURL = $window.URL.createObjectURL(file);
            newWindow.location.href = fileURL;

/*
			var urlData =  'data:text/csv;charset=UTF-8,' + encodeURIComponent(data);

		    var aLink = document.createElement('a');
		    var evt = document.createEvent("HTMLEvents");
		    evt.initEvent("click");
		    aLink.download = filename;
		    aLink.href = urlData;
		    aLink.target = '_blank'
		    aLink.dispatchEvent(evt);
*/

		};

		var toCsv = function(data, filename) {

			var rows = [];
			for(var i=0; i<data.length; i++) {
				var item = data[i];
				rows.push(itemToRow(item));

			}
			rows = rows.join('\n');
			download(rows, filename);

			//var table = (ths ? ths : '') + rows;



/*
			console.log('data', data);
			var  XLSX = $window.XLSX;
			XLSX.writeFile(data, 'out.xlsx');
*/
/*
			var table = "<table border='2px'><tr bgcolor='#87AFC6'>";
			if(ths) table += ths;
			table += rows.join('');
			table += "</table>";
*/





/*
			var sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table));

			return sa;
*/
		};


		return {
			csv: toCsv
		};

	}]);
