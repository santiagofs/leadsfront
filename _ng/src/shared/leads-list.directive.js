/* globals angular */
angular.module('app.services')
	.factory('LeadsList', ['$window', 'ApiService', function ($window, ApiService) {

		var tableSelector = null;
		var listItems = null;
		var currentPage = 1;
		//var bootstrapTable = null;

		var render = function(items) {
			for (var i = 0; i < items.length; i++) {
				items[i].timezone = items[i].timezone.toString();
			}
			listItems = items;
			$(tableSelector).bootstrapTable({
				uniqueId: 'id',
				search: true,
				pagination: true,
				pageSize: 25,
				flat: true,
				columns: [{
						field: 'status.name',
						title: 'Status',
						sortable: true,
						searchable: false,
					}, {
						field: 'name',
						title: 'Name',
						sortable: true,
						formatter: function(v, r) {
							return '<button class="btn btn-link" data-toggle="modal" data-target="#leadModal" data-id="'+r.id+'">'+v+'</button>';
						}
					}, {
						field: 'phone',
						title: 'Phone',
						sortable: true
					}, {
						field: 'source.source',
						title: 'Source',
						searchable: false,
						sortable: true
					}, {
						field: 'last_event.timelineable.headline',
						title: 'Headline',
						searchable: false,
						formatter: function(v, r) {
							return v ? v : '-';
						}
					}, {
						field: 'timezone',
						title: 'Timezone',
						sortable: true,
						searchable: false,
						editable: {
							type: 'number',
							min: -12,
							max: 12,
							params: function(params){
								console.log(params);
								return params
							}
						}
					}, {
						field: 'last_modified',
						title: 'Date',
						sortable: true,
						searchable: false,
						formatter: function(v, r) {
							return moment(v).format('MM/DD/YYYY');
							// var lastUpdate = v ? v : r.updated_at;
							// return v ? moment(lastUpdate, 'YYYY-MM-DD HH:mm:ss').format('MM/DD/YYYY') : '';
						}
					}, {
						field: 'last_event.timelineable.notes',
						title: 'Notes',
						searchable: false,
						formatter: function(v, r) {
							return r['last_event.timelineable.body'] ? r['last_event.timelineable.body'] : (v ? v : (r['notes'] ? r['notes'] : '-'));
						}
				}, {
					field: 'created_at',
					title: 'Created',
					sortable: true,
					searchable: false,
					formatter: function(v, r) {
						return v ? moment(v, 'YYYY-MM-DD HH:mm:ss').format('MM/DD/YYYY') : '';
					}
				}],
				onEditableSave: function(f,r,a,b){
					if(f === 'status_id') {

					} else {
						$(tableSelector).block();
						ApiService.lead.edit({item: r}).then(function(rsp){
							for (var i = 0, len = listItems.length; i < len; i++) {
								if(listItems[i].id === rsp.item.id){
									listItems[i] = rsp.item;
								}
							}
							$(tableSelector).unblock();
						}, console.log);
					}
				},
				data: items
			});

      $('#filters').removeClass('hide');
		}

		var init = function(selector) {
			tableSelector = selector;
		}

		var block = function(mode) {
			if(mode) {
				$(tableSelector).block();
			} else {
				$(tableSelector).unblock();
			}
		}

		var currentIndex = 0;

		function flatten(oin, oout, p) {
			for (var prop in oin) {
				if (typeof oin[prop] === 'object') {
					flatten(oin[prop], oout, prop+'.');
				} else {
					oout[p+prop] = oin[prop];
				}
			}
		}

		var updateByLeadId = function(leadId) {
			var tableOptions = $(tableSelector).bootstrapTable('getOptions');
			var tableData = $(tableSelector).bootstrapTable('getData');
			tableOptions.sortName = undefined;
			tableOptions.data = tableData;
			$(tableSelector).bootstrapTable('refreshOptions', {sortName: undefined});

			var ndx = tableData.findIndex(function(e, i, a){
				return e.id === leadId;
			});

			if(ndx !== -1) {
				$(tableSelector).block();
				ApiService.timeline.list({ filters: [{ field: 'id', value: leadId}], relations: ['status', 'lastEvent.timelineable'] }).then(function(rsp){
					// $(tableSelector).bootstrapTable('updateByUniqueId', {id: leadId, row: rsp.items[0]});
					console.log('test')
					if (rsp.items.length) {
						var flatObj = {};
						flatten(rsp.items[0], flatObj, '');

						$(tableSelector).bootstrapTable('updateByUniqueId', {id: leadId, row: flatObj});
					} else {
						$(tableSelector).bootstrapTable('removeByUniqueId', leadId);
					}

					$(tableSelector).unblock();

				}, function(rsp){
					console.log(rsp);
				});
			}

			// var nextIndex = ndx === tableData.length - 1 ? 0 : ndx+1;
			return ndx !== tableData.length - 1 ? tableData[ndx+1] : false;
		}

		function filterBy(completeFilter) {
			$(tableSelector).bootstrapTable('filterBy', completeFilter);
		}

		function getById(id) {
			return $.grep(listItems, function(r){ return r.id === id; })[0];
		}

		return {
			init: init,
			block: block,
			render: render,
			updateByLeadId: updateByLeadId,
			filterBy: filterBy,
			getById: getById,
		}

	}]);
