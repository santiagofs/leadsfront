
/* globals angular */
angular.module('app.services')
.factory('ListService', ['$q', '$rootScope', function ($q, $rootScope) {

    var list = function(getService, onGetItemsCallback) {

        var me = this;
        var ret = {};
        this.getService = getService;
        this.onGetItems = onGetItemsCallback;

        /* filters */
        ret.filters = {};
        ret.auxFilters = {};
        ret.setFilters = function(filters, auxFilters) {
            ret.filters = filters;
            me.auxFilters = auxFilters ? auxFilters : {};
        };

        me.filterMapping = {};
        var setFilterMapping = function(mapping) {
            me.filterMapping = mapping;
        };
        ret.applyFilters = function() {
            ret.getItems();
        };
        ret.resetFilters = function() {
            angular.forEach(ret.filters, function(v, k){
                ret.filters[k].value = null;
            });
            ret.getItems();
        };

        /* auxiliary lists */
        this.auxs = {};
        this.auxsMapping = {};
        var setAuxs = function(auxs, mapping) {
            me.auxs = auxs;
            me.auxsMapping = mapping;
        };

        /* sort */
        me.sort =  {field:'', mode: '+'};
        ret.setDefaultSort = function(sort) {
            me.sort = sort;
        };
        ret.getSortClass =  function(field) {
            if(me.sort.field === field) {
                return me.sort.mode === '+' ? 'sortUp' : 'sortDown';
            }
            return '';
        };
        ret.doSort = function(field) {
            me.sort.mode = me.sort.field === field ? (me.sort.mode === '+' ? '-' : '+') : '+';
            me.sort.field = field;
            ret.getItems();
        };

        /* pagination */
        ret.pagination = {
            first: 1,
            last: 0,
            next: 0,
            page: 1,
            pages: 0,
            previous: 0,
            rows: 0
        };
        var onPage = function(page) {
            me.getItems(page);
        };

        this.items = [];

        ret.setOnGetItems = function(callback) {
            me.onGetItems = callback;
        };

        ret.relations = [];
        ret.getItems = function(page) {
            var data = {
                page: page ? page : 1,
                per_page: 100,
                filters: [],
                relations: ret.relations
            };
            if(me.sort.field) data.sort =  me.sort.mode+me.sort.field;

            angular.forEach(ret.filters, function(filter, field){

                if(filter.value !== null) {
                    console.log(filter);
                    data.filters.push(filter);
                }
            });
            //console.log(me.getService)
            //$('.index-content').block();
            me.getService(data).then(
                function(rsp) {
                    ret.items = rsp.items;
                    ret.pagination = rsp.pagination;
                    console.log(rsp.items);
                    if(me.onGetItems) me.onGetItems(true, rsp);
                },
                function(err) {
                    if(me.onGetItems) me.onGetItems(false, err);
                }
            );
        };

        me.deleteService = null;
        me.deleteCallback = null;
        ret.setDeleteService = function(service, callback) {
            me.deleteService = service;
            me.deleteCallback = callback;
        };
        ret.delete = function(id, confirmMessage) {
            $rootScope.$broadcast('modal:delete', {name:confirmMessage, ng: true, cbk: function(){
            //$('.index-content').block();

                me.deleteService({id:[id]}).then(
                    function(){
                        if(me.deleteCallback) {
                            me.deleteCallback(status, id);
                        }
                        else // default remove item function
                        {
                            console.log('remove', id);
                            var k = ret.items.findIndex(function(i){return i.id === id;});
                            if (k > -1) {
                                ret.items.splice(k,1);
                            }
                        }
                        //$('.index-content').unblock();
                    },
                    function(rsp){
                        window.alert(rsp.message);
                        $('.index-content').unblock();
                    }
                );
            }});
        };

        ret.updateItem = function(item) {
            var k = ret.items.findIndex(function(i){return i.id === item.id;});
            if (k > -1) {
                ret.items[k] = item;
            } else {
                ret.items.unshift(item);
            }
        };

        this.selectAll = false;
        this.selectSome = false;
        var onSelectAll = function() {
            for(var i = 0; i < ret.items.length; i++) {
                ret.items[i].selected = me.selectAll;
            }
            me.selectSome = me.selectAll;
        };
        var onSelectItem = function() {
            var a = ret.items.length;
            var b = ret.items.filter(function(e,i,a){
                return e.selected === true;
            }).length;
            me.selectAll = (a === b);
            me.selectSome = !!b;
        };

        this.updateFieldService = null;
        this.updateFieldCallback = null;
        var setUpdateFieldService = function(service, callback) {
            me.updateFieldService = service;
            me.updateFieldCallback = callback;
        };
        var onUpdateField = function(value, field, item) {

            var data = angular.copy(item);
            data[field] = value;
            me.updateFieldService({item:data}).then(
                function(rsp) {
                    if(me.auxsMapping[field]) {
                        var mapping =me.auxsMapping[field];
                        var id = item[field];
                        var list = me.auxs[mapping.list];
                        var selected = list.filter(function(e,i,a){
                            return e.id === id;
                        });
                        var value = selected.length ? selected[0][mapping.field] : 'Not found';
                        item[mapping.target] = value;
                    }
                    return true;
                },
                function(err) {
                    return false;
                }
            );

        };



        return ret;
     };

    return list;
}]);





//     export: function() {
//         var data = {
//             page: 1,
//             per_page: 9999999,
//             sort: $scope.leadslist.sort.mode+$scope.leadslist.sort.field,
//             filters: [],
//             buyers: true,
//         };
//         angular.forEach($scope.leadslist.filters, function(filter, field){
//             if(filter !== null) {
//                 var mapped = $scope.leadslist.filterMapping[field];
//                 mapped.value = angular.isObject(filter) ? filter.id : filter;
//
//                 data.filters.push(mapped);
//             }
//         });
//
//         $('.index-content').block();
//         var deferred = $q.defer();
//         ApiService.lead.forAdmin(data).then(
//             function(rsp) {
//                 $('.index-content').unblock();
//                 deferred.resolve(rsp);
//             },
//             function(err) {
//                 $('.index-content').unblock();
//                 alert(err);
//                 deferred.reject(err);
//             }
//         );
//
//         return deferred.promise;
//     },
//     delete: function(item) {
//
//       $rootScope.$broadcast('modal:delete', {name:item.name, ng: true, cbk: function(){
//         $('.index-content').block();
//
//         ApiService.lead.delete({id:[item.id]}).then(function(){
//           var k = $scope.leadslist.items.findIndex(function(i){return i.id === item.id;});
//           if (k > -1) {
//             $scope.leadslist.items.splice(k,1);
//           }
//           $('.index-content').unblock();
//         },function(rsp){
//           window.alert(rsp.message);
//                 $('.index-content').unblock();
//             });
//       }});
//     },
