$.blockUI.defaults.message = 'Please Wait...';
$.blockUI.defaults.css.border = 'none';
$.blockUI.defaults.css.background = 'transparent';
$.blockUI.defaults.css.color = '#FFF';
$.blockUI.defaults.css.lineHeight = '50px';
$.blockUI.defaults.css.fontSize = '30px';
$.blockUI.defaults.css.fontWeight = '300';
$.blockUI.defaults.overlayCSS.minHeight = '50px';

var app = angular.module('app', [
	'ui.router',
	'xeditable',
	'templates-app',
	'app.http-services',
	'app.models',
	'app.directives',
	'app.services',
	'app.directives',
	'app.filters',
	'app.navigation',
	'app.login',
	'app.leads',
	'app.lead.edit',
	'app.lead.import',
	'app.lead.reassign',
	'app.buyers',
	'app.schedule',
	'app.notifications',
	'app.notification.edit',
	'app.notes',
	'app.campaigns',
	'app.campaign.edit',
	'app.sources',
	'app.source.edit',
	'app.agents',
	'app.agent.edit',
	'app.reports',
	'app.modal.delete',
]);

app.config(function ($qProvider, $stateProvider, $locationProvider, $urlRouterProvider) {
	$qProvider.errorOnUnhandledRejections(false);

	function auth(UserService, $state, $timeout) {
		if (!UserService.isLogged()) {
			$timeout(function () {
				$state.go('login');
			});
		}
	}
	$stateProvider.state('index', {
		url: '/',
		redirectTo: 'dashboard',
	})
	.state('login', {
		url: '/login',
		templateUrl: '../src/modules/login.tpl.html'
	})

	// AGENT ROUTES
	.state('agent', {
		name: 'agent',
		template: '<ui-view>Loading...</ui-view>',
		redirectTo: 'dashboard',
		perms: 4
	})
	.state('agent.leads', {
		url: '/leads',
		templateUrl: '../src/modules/agent/agent.leads.tpl.html',
		perms: 4
	})
	.state('agent.buyers', {
		url: '/buyers',
		templateUrl: '../src/modules/agent/agent.buyers.tpl.html',
		perms: 4
	})
	.state('agent.scheduled', {
		url: '/scheduled',
		templateUrl: '../src/modules/agent/agent.schedule.tpl.html',
		perms: 4
	})

	.state('agent.notifications', {
		url: '/notifications',
		templateUrl: '../src/modules/notifications.tpl.html',
		perms: 4
	})
	.state('agent.notes', {
		url: '/notes',
		templateUrl: '../src/modules/notes.tpl.html',
		perms: 4
	})

	// ADMIN ROUTES
	.state('admin', {
		//abstract: true,
		name: 'admin',
		url: '/admin',
		redirectTo: 'dashboard',
		template: '<ui-view>Loading...</ui-view>',
		perms: 2
	})
		.state('admin.campaigns', {
			//abstract: true,
			url: '/campaigns',
			template: '<ui-view></ui-view>',
			redirectTo: 'admin.campaigns.list',
			perms: 2
		})
			.state('admin.campaigns.list', {
				url: '/campaigns',
				templateUrl: '../src/modules/admin/campaigns/list.tpl.html',
				perms: 2,
				pageTitle: 'Campaign List',
			})
			.state('admin.campaigns.edit', {
				url: '/{campaignId}',
				templateUrl: '../src/modules/admin/campaigns/edit.tpl.html',
				perms: 2,
				pageTitle: 'Campaign Edit',
			})
		.state('admin.sources', {
			//abstract: true,
			url: '/sources',
			template: '<ui-view></ui-view>',
			redirectTo: 'admin.sources.list',
			perms: 2
		})
			.state('admin.sources.list', {
				url: '/sources',
				templateUrl: '../src/modules/admin/sources/list.tpl.html',
				perms: 2,
				pageTitle: 'Sources List',
			})
			.state('admin.sources.edit', {
				url: '/{sourceId}',
				templateUrl: '../src/modules/admin/sources/edit.tpl.html',
				perms: 2,
				pageTitle: 'Source Edit',
			})
		.state('admin.leads', {
			//abstract: true,
			url: '/leads',
			templateUrl: '../src/modules/admin/leads/index.tpl.html',
			redirectTo: 'admin.leads.list',
			perms: 2
		})
			.state('admin.leads.list', {
				url: '/list',
				templateUrl: '../src/modules/admin/leads/list.tpl.html',
				perms: 2,
				pageTitle: 'Leads List'
			})
			.state('admin.leads.import', {
				url: '/import',
				templateUrl: '../src/modules/admin/leads/import.tpl.html',
				perms: 2,
				pageTitle: 'Import Leads',
			})
			.state('admin.leads.reassign', {
				url: '/reassign',
				templateUrl: '../src/modules/admin/leads/reassign.tpl.html',
				pageTitle: 'Reassing Leads'
			})
			.state('admin.leads.edit', {
				url: '/{leadId}',
				templateUrl: '../src/modules/admin/leads/lead.tpl.html',
				redirectTo: 'admin.leads.edit.form',
				perms: 2
			})
			.state('admin.leads.edit.form', {
				url: '/form',
				templateUrl: '../src/modules/admin/leads/lead-form.tpl.html',
				perms: 2,
				pageTitle: 'Leads Edit',
			})
			.state('admin.leads.edit.sold', {
				url: '/sold',
				templateUrl: '../src/modules/admin/leads/lead-sold.tpl.html',
				perms: 2,
				pageTitle: 'Leads Sold History',
			})
			.state('admin.leads.edit.history', {
				url: '/history',
				templateUrl: '../src/modules/admin/leads/lead-history.tpl.html',
				perms: 2,
				pageTitle: 'Leads History',
			})
		.state('admin.buyers', {
			url: '/buyers',
			templateUrl: '../src/modules/admin/leads/buyers.tpl.html',
			perms: 2,
			redirectTo: 'admin.buyers.list',
		})
			.state('admin.buyers.list', {
				url: '/list',
				templateUrl: '../src/modules/admin/leads/buyers-list.tpl.html',
				perms: 2,
				pageTitle: 'Buyers List'
			})
		.state('admin.sales', {
			url: '/sales',
			templateUrl: '../src/modules/admin/sales/index.tpl.html',
			perms: 2,
			redirectTo: 'admin.sales.list',
		})
			.state('admin.sales.list', {
				url: '/list',
				templateUrl: '../src/modules/admin/sales/list.tpl.html',
				perms: 2,
				pageTitle: 'Sales List'
			})
			.state('admin.sales.edit', {
				url: '/{id}',
				templateUrl: '../src/modules/admin/sales/edit.tpl.html',
				perms: 2,
				pageTitle: 'Sales Edit'
			})
		.state('admin.notifications', {
			url: '/notifications',
			templateUrl: '../src/modules/admin/notifications/index.tpl.html',
			perms: 2,
			pageTitle: 'Notifications List',
		})
			.state('admin.notification-edit', {
				url: '/notification/{notificationId}',
				templateUrl: '../src/modules/admin/notifications/edit.tpl.html',
				perms: 2,
				pageTitle: 'Notifications Edit',
			})
		.state('admin.notes', {
			url: '/notes',
			templateUrl: '../src/modules/notes.tpl.html',
			perms: 2,
			pageTitle: 'Notes List',
		})
		.state('admin.agents', {
			url: '/agents',
			templateUrl: '../src/modules/admin/agents/index.tpl.html',
			perms: 2,
			pageTitle: 'Agents Lista',
		})
			.state('admin.agent-edit', {
				url: '/agent/{agentId}',
				templateUrl: '../src/modules/admin/agents/edit.tpl.html',
				perms: 2,
				pageTitle: 'Agents Edit',
			})
		.state('admin.report-agents', {
			url: '/report-agents',
			templateUrl: '../src/modules/admin/reports/agents.tpl.html',
			perms: 2,
			pageTitle: 'Reports',
		})
		.state('admin.report-leads', {
			url: '/report-leads',
			templateUrl: '../src/modules/admin/reports/leads.tpl.html',
			perms: 2,
			pageTitle: 'Reports',
		})

	// other common routes
	.state('dashboard', {
		url: '/dashboard',
		templateUrl: '../src/modules/dashboard.tpl.html',
		perms: 5,
		pageTitle: 'Dashboard',
	})
	.state('403', {
		url: '/403',
		templateUrl: '../src/modules/403.tpl.html'
	})
	.state('catchall', {
		url: "*path",
		templateUrl: '../src/modules/404.tpl.html',
		pageTitle: 'Page Not Found',
	});
	$locationProvider.html5Mode(true);


}).run(['UserService','editableOptions', function (UserService, editableOptions) {
	UserService.getStoredUser();
	editableOptions.theme = 'bs3';
}]);
var config_data = window.leads_config !== undefined ? window.leads_config : {};
app.constant('LEAD_CONFIG', config_data);
app.controller('AppController', [
	'UserService', '$scope', '$state', 'ExportService',
	function (UserService, $scope, $state, ExportService) {
		// LOGIN - LOGOUT
		$scope.user = UserService.current();
		$scope.$on('user:login', function (e, user) {
			$scope.user = user;
		});
		$scope.logout = function () {
			UserService.logout();
			$state.go('login');
		}
		//$scope.$state = $state;
		// REDIRECTION FUNCTIONS ON PERMS
		$scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
			// console.log('>>> $stateChangeStart');
			if (toState.redirectTo) {
				event.preventDefault();
				// console.log('simple redirect', toState.redirectTo);
				$state.go(toState.redirectTo, toParams, {
					location: 'replace'
				});
				return false;
			}
			// console.log('>>> toState.perms', toState.perms);
			// console.log('>>> $scope.user', $scope.user);
			if (toState.perms && !$scope.user) {
				event.preventDefault();
				// console.log('login redirect')
				$state.go('login', null, {
					location: 'replace'
				});
				return false;
			}
			if (toState.perms && $scope.user && toState.perms < $scope.user.role_id) {
				event.preventDefault();
				// console.log('not allowed redirect')
				$state.go('dashboard', null, {
					location: 'replace'
				});
				return false;
			}
			$scope.pageTitle = toState.pageTitle ? toState.pageTitle : '';
		});
		$scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
			$scope.previousState = fromState;
		});
		// GLOBAL EXPORT FUNCTION
		$scope.globalCsvExport = function (list, headers) {
			//console.log('>>> global export list', list, headers);
			ExportService.csv(list, headers);
		}
	}
]);
angular.module('app.http-services', []);
angular.module('app.models', []);
angular.module('app.directives', []);
angular.module('app.services', []);
angular.module('app.directives', []);
angular.module('app.filters', []);
angular.module('app.navigation', []);
angular.module('app.login', []);
angular.module('app.leads', []);
angular.module('app.lead.edit', []);
angular.module('app.lead.import', []);
angular.module('app.lead.reassign', []);
angular.module('app.buyers', []);
angular.module('app.schedule', []);
angular.module('app.notifications', []);
angular.module('app.notification.edit', []);
angular.module('app.notes', []);
angular.module('app.campaigns', []);
angular.module('app.campaign.edit', []);
angular.module('app.sources', []);
angular.module('app.source.edit', []);
angular.module('app.agents', []);
angular.module('app.agent.edit', []);
angular.module('app.reports', []);
angular.module('app.modal.delete', []);
