var navigation = {
  agent: [
	{
      title: 'Dashboard',
      slug: 'dashboard'
    },
    {
      title: 'Leads',
      slug: 'leads'
    },
    {
      title: 'Buyers',
      slug: 'buyers'
    },
    {
      title: 'Scheduled Calls',
      slug: 'scheduled'
    },
    {
      title: 'Notifications',
      slug: 'notifications'
    }
  ],
  admin: [
	{
      title: 'Dashboard',
      slug: 'dashboard'
    },
    {
      title: 'Campaigns',
      slug: 'campaigns'
    },
    {
      title: 'Sources',
      slug: 'sources'
    },
    {
      title: 'Leads',
      slug: 'leads'
    },
    {
      title: 'Buyers',
      slug: 'buyers'
    },
    {
      title: 'Sales',
      slug: 'sales'
    },
    {
      title: 'Notifications',
      slug: 'notifications'
    },
    {
      title: 'Agents',
      slug: 'agents'
    },
    {
      title: 'Reports',
      slug: 'report-agents'
    },
  ]
}

angular.module('app.navigation', [])
.config(function config() {

})
.controller('NavigationController', ['$scope', 'UserService', '$state', 'ApiService', '$interval', function($scope, UserService, $state, ApiService, $interval){
	$scope.getUser = UserService.current;
	$scope.links = navigation;

	$scope.getStateName = function(tab) {
		if(tab === 'dashboard') return tab;
		return $scope.user.role + '.' + tab;
	};
	$scope.isActive = function(check) {
		var root = check === 'dashboard' ? check : $scope.user.role + '.' + check;
		return $state.current.name.indexOf(root) !== -1;
	};
	var now = moment().format();

	//console.log('utc', now.utc());
	$scope.nextSchedules = 0;
	var getNextSchedules = function() {
		ApiService.schedule.pending({filters:[
			{field: 'user_id', value: $scope.user.id},
			{field: 'call_date', op: '>=', value: now},
		]}).then(function(rsp){
			$scope.nextSchedules = rsp.items.length;
		})
	}
	$interval(getNextSchedules, 1*60*1000); //cada un minuto
	getNextSchedules();






}]);
