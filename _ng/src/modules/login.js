angular.module('app.login').controller('LoginCtrl', ['$scope', '$state', 'ApiService', 'UserService', function($scope, $state, ApiService, UserService){

  $scope.login = {
    email: '',
    password: '',
  }

  $scope.loginResponse = null;

  $scope.doLogin = function() {
    var data = $scope.login;
    $scope.loginResponse = null;

    ApiService.user.login(data).then(function(rsp){

			UserService.set(rsp.user);
			$state.go('index');
		},
	    function(rsp){
		  console.log(rsp);
	      $scope.loginResponse = rsp.message;
	      //$scope.$apply();
    });

  }
}]);
