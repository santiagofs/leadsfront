angular.module('app.campaign.edit')
.controller('SourceEditController', ['$state', '$scope', 'ApiService', 'SourceModel', function($state, $scope, ApiService, SourceModel){

  var sourceId = $state.params.sourceId === 'new' ? null : $state.params.sourceId;


  if(sourceId) {
    $('form').block();
    ApiService.source.get({ id : sourceId }).then(function(rsp){
      $scope.source = rsp.item;
      $('form').unblock();
    },function(rsp){
      $scope.source = SourceModel.empty();
      $('form').unblock();
    })
  } else {
    $scope.source = SourceModel.empty();
  }

  $scope.saveSource = function(){
    ApiService.source.edit({ item: $scope.source }).then(function(rsp){
      $state.go('admin.sources');
    },function(rsp){
      console.log(rsp);
    });
  }
}]);
