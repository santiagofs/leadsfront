angular.module('app.sources')
.controller('AdminSourcesListController', ['$scope', '$rootScope', 'ApiService', function($scope, $rootScope, ApiService){

  $('.sources-table').block();

  ApiService.source.list().then(
    function(rsp){
      $('.sources-table').bootstrapTable({
        columns: [{
          checkbox: true
        },{
          field: 'source',
          title: 'Source',
          sortable: true,
          formatter: function(v, r) {
            return '<a href="admin/sources/'+r.id+'">'+v+'</a>';
          }
        },{
          field: 'id',
          title: 'Actions',
          formatter: function(v, r) {
            return '<a href="admin/sources/'+v+'" class="btn-primary btn-xs glyphicon glyphicon-pencil"></a> '+
            '<button data-id="'+v+'" data-name="'+r.source+'" class="btn-danger btn-xs glyphicon glyphicon-remove row-delete"></button>';
          }
        }],
        data: rsp.items
      });

      $('#filters').removeClass('hide');
      $('.sources-table').unblock();
    },
    function(rsp){
      console.log('An error ocurred. Please try again later.');
      $('.sources-table').unblock();
    }
  );

  $('.sources-table').on('click', '.row-delete', function(e){
    var name = $(this).data('name');
    var id = $(this).data('id');
    $rootScope.$broadcast('modal:delete', {name:name, cbk: function(){
      $('.sources-table').block();
      ApiService.source.delete({id:id}).then(function(){
        $('.sources-table').bootstrapTable('remove', {field: 'id', values:[id]})
        $('.sources-table').unblock();
      },function(rsp){
        window.alert(rsp.message);
				$('.sources-table').unblock();
			});
    }});
  });

  $scope.selectedRows = [];

  $('.sources-table').on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function(e){
    $scope.selectedRows = $('.sources-table').bootstrapTable('getSelections');
    $scope.$digest();
  });

  $scope.modalMassDeleteShow = function() {
    var name = $scope.selectedRows.length+' items';
    var ids = $scope.selectedRows.map(function(e,i,a){
      return e.id;
    });

    $rootScope.$broadcast('modal:delete', {name:name, ng: true, cbk: function(){
      $('.sources-table').block();
      ApiService.source.delete({id:ids}).then(function(){
        $('.sources-table').bootstrapTable('remove', {field: 'id', values:ids});
        $('.sources-table').unblock();
      },function(rsp){
        window.alert(rsp.message);
        $('.sources-table').unblock();
      });
    }});
  };
}]);
