angular.module('app.leads')
    .controller('AdminSalesEditCtrl',
        ['$state', '$scope', 'ApiService',
        function($state, $scope, ApiService){

            $scope.item = null;
            ApiService.sale.get({
                id: $state.params.id,
                relations: ['campaign', 'user', 'lead']
            }).then(
                function(rsp) {
                    $scope.item = rsp.item;
                    console.log(rsp);
                },
                function(err) {
                    window.alert(err);
                }
            );

            $scope.saveSale = function() {
                ApiService.sale.edit({item: $scope.item}).then(
                    function(rsp) {
                        var parsed = rsp.item;

                        parsed.agent = rsp.item.user ? rsp.item.user.name : '';
                        parsed.campaign = rsp.item.campaign ? rsp.item.campaign.name : '';
                        parsed.lead = rsp.item.lead ? rsp.item.lead.name : '';

                        $scope.salesList.updateItem(parsed);

                        $state.go('admin.sales');
                    },
                    function(err) {
                        window.alert(err);
                    }
                );
            }

        }]
    );
