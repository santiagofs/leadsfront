angular.module('app.leads')
    .controller('AdminSalesCtrl',
        ['$scope', 'ListService', 'ApiService',
        function($scope, ListService, ApiService){


            $scope.auxs = {
                agents: [],
                campaigns: []
            }
            ApiService.lead.auxs().then(
                function(rsp) {
                    console.log(rsp)
                    $scope.auxs.agents = rsp.agents;
                    $scope.auxs.campaigns  = rsp.campaigns;
                    console.log('>>> the auxs')
                },
                function(err) {
                    alert(err);
                }
            );


            $scope.salesList = new ListService(ApiService.sale.list, function(status){
                console.log(status);
                console.log($scope.salesList.pagination);

                // if(status) {
                //     console.log(status);
                //     $scope.$apply();
                // }
            });
            $scope.salesList.relations = ['campaign', 'lead', 'user'];
            $scope.salesList.setDefaultSort({field:'created_at', mode: '-'});
            //text: {field: 'text', 'op': 'like', value: '', skip: true},
            $scope.salesList.setFilters({
                lead: {field: 'leads.name', 'op': 'like', value: null},
                agent: {field: 'users.id', 'op': '=', value: null},
                campaign: {field: 'campaign.id', 'op': '=', value: null},
            });
            $scope.salesList.setOnGetItems(function(a,b){
                console.log('callback')
            });
            $scope.salesList.setDeleteService(ApiService.sale.delete)
            $scope.salesList.getItems();

        }]
    );
