angular.module('app.campaigns')
.controller('CampaignsController', ['$scope', '$rootScope', 'ApiService', function($scope, $rootScope, ApiService){

  $('.campaigns-table').block();

  ApiService.campaign.list().then(
    function(rsp){
      $('.campaigns-table').bootstrapTable({
        columns: [{
          checkbox: true
        },{
          field: 'name',
          title: 'Name',
          sortable: true,
          formatter: function(v, r) {
            return '<a href="admin/campaigns/'+r.id+'">'+v+'</a>';
          }
        },{
          field: 'start_date',
          title: 'Start Date',
          sortable: true,
          formatter: function(v) {
            return moment(v).format('DD MMM YYYY');
          }
        },{
          field: 'end_date',
          title: 'End Date',
          sortable: true,
          formatter: function(v) {
            return moment(v).format('DD MMM YYYY');
          }
        },{
          field: 'active',
          title: 'Active',
          sortable: true,
          formatter: function(v, r) {
            var chd = !!parseInt(v) ? 'checked' : '';
            return '<label class="input-toggle">'+
                '<input class="input-toggle-hidden" type="checkbox" name="active" data-id="'+r.id+'" '+chd+'>'+
                '<span class="input-toggle-mask"></span>'+
                '</label>';
          }
        },{
          field: 'id',
          title: 'Actions',
          formatter: function(v, r) {
            return '<a href="admin/campaigns/'+v+'" class="btn-primary btn-xs glyphicon glyphicon-pencil"></a> '+
            '<button data-id="'+v+'" data-name="'+r.name+'" class="btn-danger btn-xs glyphicon glyphicon-remove row-delete"></button>';
          }
        }],
        data: rsp.items
      });

      $('#filters').removeClass('hide');
      $('.campaigns-table').unblock();
    },
    function(rsp){
      console.log('An error ocurred. Please try again later.');
      $('.campaigns-table').unblock();
    }
  );

  $('.campaigns-table').on('change', '[name=active]', function(e){
    var campaignItem = {
      id: $(this).data('id'),
      active: $(this).is(':checked') ? 1 : 0
    };
    ApiService.campaign.edit({ item : campaignItem }).then(console.log, console.log);
  });

  $('.campaigns-table').on('click', '.row-delete', function(e){
    var name = $(this).data('name');
    var id = $(this).data('id');
    $rootScope.$broadcast('modal:delete', {name:name, cbk: function(){
      $('.campaigns-table').block();
      ApiService.campaign.delete({id:id}).then(function(){
        $('.campaigns-table').bootstrapTable('remove', {field: 'id', values:[id]});
        $('.campaigns-table').unblock();
      },function(rsp){
        window.alert(rsp.message);
				$('.campaigns-table').unblock();
			});
    }});
  });

  $scope.selectedRows = [];

	$('.campaigns-table').on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function(e){
		$scope.selectedRows = $('.campaigns-table').bootstrapTable('getSelections');
		$scope.$digest();
	});

	$scope.modalMassDeleteShow = function() {
    var name = $scope.selectedRows.length+' items';
		var ids = $scope.selectedRows.map(function(e,i,a){
			return e.id;
		});

    $rootScope.$broadcast('modal:delete', {name:name, ng: true, cbk: function(){
      $('.campaigns-table').block();
      ApiService.campaign.delete({id:ids}).then(function(){
        $('.campaigns-table').bootstrapTable('remove', {field: 'id', values:ids});
        $('.campaigns-table').unblock();
      },function(rsp){
        window.alert(rsp.message);
				$('.campaigns-table').unblock();
			});
    }});
	};
}]);
