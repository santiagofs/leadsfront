angular.module('app.campaign.edit')
.controller('CampaignEditController', ['$state', '$scope', 'ApiService', 'CampaignModel', function($state, $scope, ApiService, CampaignModel){

  var campaignId = $state.params.campaignId === 'new' ? null : $state.params.campaignId;


  if(campaignId) {
    $('form').block();
    ApiService.campaign.get({ id : campaignId }).then(function(rsp){
      $scope.campaign = rsp.item;
      $scope.activeInput = !!parseInt($scope.campaign.active);
      $('form').unblock();
    },function(rsp){
      $scope.campaign = CampaignModel.empty();
      $('form').unblock();
    })
  } else {
    $scope.campaign = CampaignModel.empty();
  }

  $('.datepicker').datetimepicker({
    format: 'YYYY-MM-DD'
  }).on('dp.change', function(e){
    var $dp = $(e.currentTarget);
    $scope.campaign[$dp.attr('name')] = $dp.val();
  });

  $scope.toggleActive = function (){
    $scope.campaign.active = $scope.activeInput ? 1 : 0;
  }

  $scope.saveCampaign = function(){
    ApiService.campaign.edit({ item: $scope.campaign }).then(function(rsp){
      $state.go('admin.campaigns');
    },function(rsp){
      console.log(rsp);
    });
  }
}]);
