angular.module('app.agent.edit')
.controller('AgentEditController', ['$state', '$scope', 'ApiService', 'UserModel', function($state, $scope, ApiService, UserModel){

  var agentId = $state.params.agentId === 'new' ? null : $state.params.agentId;
  $scope.agentId = agentId;
  $scope.newPassword = {
    id: agentId,
    password: ''
  };
  
  $scope.roles = [
	  {id: 4, label: 'Agent'},
	  {id: 3, label: 'Loader'},
	  {id: 2, label: 'Admin'}
  ]

  if(agentId) {
    $('form').block();
    ApiService.user.get({ id : agentId }).then(function(rsp){
      $scope.agent = rsp.user;
      $scope.activeInput = !!parseInt($scope.agent.active);
      $('form').unblock();
    },function(rsp){
      $scope.agent = UserModel.empty();
      $('form').unblock();
    })
  } else {
    $scope.agent = UserModel.empty();
    console.log($scope.agent);
  }

/*
  $scope.toggleActive = function (){
    $scope.agent.active = $scope.activeInput ? 1 : 0;
  }
*/

  $scope.saveUser = function(){
    if ($scope.form.$valid) {
      ApiService.user.edit({ user: $scope.agent }).then(function(rsp){
        $state.go('admin.agents');
      },function(rsp){
        console.log(rsp);
      });
    }
  };

  $scope.savePassword = function(){
    if ($scope.passwordForm.$valid) {
      ApiService.user.edit({ user: $scope.newPassword }).then(function(rsp){
        $state.go('admin.agents');
      },function(rsp){
        console.log(rsp);
      });
    }
  };
}]);
