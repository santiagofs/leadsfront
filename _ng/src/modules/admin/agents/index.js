angular.module('app.agents')
.controller('AgentsController', ['$scope', '$rootScope', 'ApiService', function($scope, $rootScope, ApiService){

    $('.agents-table').block();

    var query = {
      filters: [
        { field: 'role_id', 'op': '>=', value: '2' },
        // { field: 'role_id', value: '4', mode: 'orWhere' }
      ],
      relations: ['role']
    };

    
    ApiService.user.list(query).then(function(rsp){
      $('.agents-table').bootstrapTable({
        columns: [{
          checkbox: true
        },{
          field: 'name',
          title: 'Name',
          sortable: true,
          formatter: function(v, r){
            return '<a href="admin/agent/'+r.id+'">'+v+'</a>';
          }
        }, {
          field: 'email',
          title: 'Email',
          sortable: true
        }, {
          field: 'active',
          title: 'Active',
          sortable: true,
          formatter: function(v, r) {
            var chd = !!parseInt(v) ? 'checked' : '';
            return '<label class="input-toggle">'+
                '<input class="input-toggle-hidden" type="checkbox" name="active" data-id="'+r.id+'" '+chd+'>'+
                '<span class="input-toggle-mask"></span>'+
                '</label>';
          }
        },{
          field: 'id',
          title: 'Actions',
          formatter: function(v, r) {
            return '<a href="admin/agent/'+v+'" class="btn-primary btn-xs glyphicon glyphicon-pencil"></a> '+
            '<button data-id="'+v+'" data-name="'+r.name+'" class="btn-danger btn-xs glyphicon glyphicon-remove row-delete"></button>';
          }
        }],
        data: rsp.users
      });

      $('#filters').removeClass('hide');
      $('.agents-table').unblock();
    },
    function(rsp){
      console.log('An error ocurred. Please try again later.');
      $('.agents-table').unblock();
    }
  );

  $('.agents-table').on('click', '.row-delete', function(e){
    var name = $(this).data('name');
    var id = $(this).data('id');
    $rootScope.$broadcast('modal:delete', {name:name, cbk: function(){
      $('.agents-table').block();
      ApiService.user.delete({id:id}).then(function(){
        $('.agents-table').bootstrapTable('remove', {field: 'id', values:[id]})
        $('.agents-table').unblock();
      },function(rsp){
        window.alert(rsp.message);
				$('.agents-table').unblock();
			});
    }});
  });

  $('.agents-table').on('change', '[name=active]', function(e){
    var userItem = {
      id: $(this).data('id'),
      active: $(this).is(':checked') ? 1 : 0
    };
    ApiService.user.edit({ item : userItem }).then(console.log, console.log);
  });

  $scope.selectedRows = [];

	$('.agents-table').on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function(e){
		$scope.selectedRows = $('.agents-table').bootstrapTable('getSelections');
		$scope.$digest();
	});

	$scope.modalMassDeleteShow = function() {
    var name = $scope.selectedRows.length+' items';
		var ids = $scope.selectedRows.map(function(e,i,a){
			return e.id;
		});

    $rootScope.$broadcast('modal:delete', {name:name, ng: true, cbk: function(){
      $('.agents-table').block();
      ApiService.user.delete({id:ids}).then(function(){
        $('.agents-table').bootstrapTable('remove', {field: 'id', values:ids});
        $('.agents-table').unblock();
      },function(rsp){
        window.alert(rsp.message);
				$('.agents-table').unblock();
			});
    }});
	};
}]);
