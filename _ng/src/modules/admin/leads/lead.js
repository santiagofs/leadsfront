angular.module('app.lead.edit')
.controller('LeadEditController', ['$state', '$scope', 'ApiService', 'LeadModel', function($state, $scope, ApiService, LeadModel){

  var leadId = $state.params.leadId === 'new' ? null : $state.params.leadId;
  $scope.agents = [];

  ApiService.user.list().then(function(rsp){
    var selectOptions = [];
      $scope.agents = rsp.users;
    // for(var i = 0; i < rsp.users.length; i++) {
    //   var user = rsp.users[i];
    //   selectOptions.push({
    //     id: user.id,
    //     text: user.name
    //   });
    // }
    //
    // var $agentsSelect = $('.agents-select');
    // $agentsSelect.select2({
    //   placeholder: "Agent",
    //   data: selectOptions
    // });
    //
    // $agentsSelect.on('select2:select', function(){
    //   $scope.lead.user_id = $(this).val();
    // });
    //
    // $agentsSelect.siblings('.select2').find('.select2-selection').on('focus', function(){
    //   $agentsSelect.select2('open');
    // });

  },function(rsp){
    console.log(rsp);
  });

  $scope.sources = [];
  ApiService.source.list().then(function(rsp){
    var selectOptions = [];

    $scope.sources = rsp.items;
    // for(var i = 0; i < rsp.items.length; i++) {
    //   var item = rsp.items[i];
    //   selectOptions.push({
    //     id: item.id,
    //     text: item.source
    //   });
    // }
    //
    // var $agentsSelect = $('.sources-select');
    // $agentsSelect.select2({
    //   placeholder: "Source",
    //   data: selectOptions
    // });
    //
    // $agentsSelect.on('select2:select', function(){
    //   $scope.lead.source_id = $(this).val();
    // });
    //
    // $agentsSelect.siblings('.select2').find('.select2-selection').on('focus', function(){
    //   $agentsSelect.select2('open');
    // });

  },function(rsp){
    console.log(rsp);
  });

  if(leadId) {
    $('form').block();
    ApiService.lead.get({ id : leadId }).then(function(rsp){
      $scope.lead = rsp.item;
      $('form').unblock();
    },function(rsp){
      $scope.lead = LeadModel.empty();
      $scope.lead.type_id = 1;
      $scope.lead.status_id = 0;
      $('form').unblock();
    })
  } else {
    $scope.lead = LeadModel.empty();
    $scope.lead.type_id = 1;
    $scope.lead.status_id = 0;
  }

  $scope.saveLead = function(){
    ApiService.lead.edit({ item: $scope.lead }).then(function(rsp){
        var parsed = rsp.item;
        parsed.campaign = rsp.item.campaign ? rsp.item.campaign.name : null;
        parsed.status = rsp.item.status ? rsp.item.status.name : null;
        parsed.source = rsp.item.source ? rsp.item.source.source : null;
        parsed.agent = rsp.item.user ? rsp.item.user.name : null;

      $scope.leadslist.onUpdateItem(parsed);
      console.log($scope.previousState);
      if($scope.previousState.name) {
        $state.go($scope.previousState.name);
      } else {
        $state.go('admin.leads');
      }
      
    },function(rsp){
      console.log(rsp);
    });
  }
}]);
