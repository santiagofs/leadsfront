angular.module('app.leads')
.controller('AdminLeadsListCtrl', ['$scope', '$rootScope', 'ApiService', 'UserService', 'ExportService', function($scope, $rootScope, ApiService, UserService, ExportService){

// 	//$('.leads-table').block();
//
// 	var agentSelectOptions = [];
// 	ApiService.user.list({
// 		filters:[
// 			{field: 'role_id', 'op': '>=', value: 3}
// 		]
// 	}).then(function(rsp){
// 		agentSelectOptions = rsp.users.map(function(e,i,a){
// 			return {value: e.id, text: e.name}
// 		})
// 	});
//
//   $scope.filterOptions = {
//     source: [],
//     status: [],
//     timezone: [],
//     campaign: [],
//     agent: [],
//   }
//
//   $scope.filter = {
//     source: null,
//     'status.name': null,
//     timezone: null,
//     'campaign.name': null,
//     'user.name': null,
//   }
//
//   $scope.makeFilter = function(){
//     var completeFilter = {};
//     $scope.filtersCleaneable = false;
//
//     for (var prop in $scope.filter) {
//       if ($scope.filter[prop]) {
//         $scope.filtersCleaneable = true;
//         completeFilter[prop] = $scope.filter[prop];
//       }
//     }
//
//     //$('.leads-table').bootstrapTable('filterBy', completeFilter);
//   };
//
// 	$scope.cleanFilters = function(){
// 		for (var prop in $scope.filter) {
// 		$scope.filter[prop] = null;
// 		}
//
// 		$scope.makeFilter();
// 	};
//
// 	$scope.leadsList = [];
// 	var onLeadsReceived = function(rsp) {
// 		console.log(rsp);
// 	};
//
// 	ApiService.lead.list(
// 		{relations: ['user', 'status', 'lastEvent.timelineable', 'campaign', 'source'], per_page: 100}
// 	).then(function(rsp){
// 		//console.log(rsp);
// 		//Status, Name, Phone, Time Zone, Agent, Date Modified, area for notes or incidents
//
// 		onLeadsReceived(rsp)
// 		// for (var i = 0; i < rsp.items.length; i++) {
// 		// 	var lead = rsp.items[i];
// 		//
// 		// 	if(lead.source) {
// 		// 		if ($scope.filterOptions.source.indexOf(lead.source.source) < 0) {
// 		// 			$scope.filterOptions.source.push(lead.source.source);
// 		// 		}
// 		// 	}
// 		//
// 		// 	if ($scope.filterOptions.status.indexOf(lead.status.name) < 0) {
// 		// 		$scope.filterOptions.status.push(lead.status.name);
// 		// 	}
// 		// 	if ($scope.filterOptions.timezone.indexOf(lead.timezone) < 0) {
// 		// 		$scope.filterOptions.timezone.push(lead.timezone);
// 		// 	}
// 		// 	if(lead.campaign) {
// 		// 		if ($scope.filterOptions.campaign.indexOf(lead.campaign.name) < 0) {
// 		// 			$scope.filterOptions.campaign.push(lead.campaign.name);
// 		// 		}
// 		// 	}
// 		// 	if(lead.user) {
// 		// 		if ($scope.filterOptions.agent.indexOf(lead.user.name) < 0) {
// 		// 			$scope.filterOptions.agent.push(lead.user.name);
// 		// 		}
// 		// 	}
// 		// }
//
// //       $('.leads-table').bootstrapTable({
// //           pagination: true,
// //           pageSize: 25,
// //           search: true,
// // 					uniqueId: 'id',
// //           flat: true,
// //           onEditableSave: function(f,r,o,$e){
// // 						$('.leads-table').block();
// // 						ApiService.lead.edit({item: r}).then(function(){
// // 							$('.leads-table').unblock();
// // 						}, function(rsp){
// // 							window.alert(rsp.message);
// // 							$('.leads-table').unblock();
// // 						});
// //           },
// //           columns: [{
// // 	          checkbox: true
// // 	        },{
// //             field: 'status_id',
// //             title: 'Status',
// //             sortable: true,
// //             searchable: true,
// //             editable: {
// //                 type: 'select',
// //                 source: [
// //                     {value: 1, text: 'Attempting Contact'},
// //                     {value: 3, text: 'DNC Not Interested'},
// //                     {value: 4, text: 'DNC Wrong or Invalid Number'},
// //                     {value: 5, text: 'Pitched'},
// //                     {value: 6, text: 'New'},
// //                     {value: 7, text: 'Buyer'},
// // 					// {value: 7, text: 'Buyer'}
// //                 ],
// //                 //'title': 'Herd Tag',
// //                 //'prepend': {none: "--------------"}
// //
// //             }
// //           }, {
// //             field: 'name',
// //             title: 'Name',
// //             sortable: true,
// //             formatter: function(v, r) {
// //               return '<a href="admin/leads/'+r.id+'">'+v+'</a>';
// //             }
// //           }, {
// //             field: 'phone',
// //             title: 'Phone',
// //             sortable: true,
// //             editable: true,
// //           },/*
// //  {
// //             field: 'email',
// //             title: 'E-Mail',
// //             sortable: true,
// //             editable: true,
// //           },
// // */ {
// //             field: 'timezone',
// //             title: 'Timezone',
// //             sortable: true,
// //             searchable: false,
// //             editable: true,
// //           }, {
// //             field: 'user_id',
// //             title: 'Agent',
// //             sortable: true,
// //             searchable: true,
// //             editable: {
// //                 type: 'select',
// //                 source: agentSelectOptions,
// //             },
// //
// //             formatter: function(v) {
// //               return v ? v : '-';
// //             }
// //           }, {
// //             field: 'source.source',
// //             title: 'Source',
// //             sortable: true,
// // 						searchable: false,
// //             //editable: true,
// //           }, {
// // 	        field: 'campaign.name',
// //             title: 'Campaign',
// //             sortable: true,
// //             searchable: false,
// //           },/*
// //  {
// //             field: 'notes',
// //             title: 'Notes',
// //             searchable: false,
// //             editable: true,
// //             cellStyle: function() {
// //               return {css: {'min-width': '300px'}}
// //             },
// //           },
// // */ {
// //             field: 'last_event',
// //             title: 'Date Modified',
// //             sortable: true,
// //             searchable: false,
// //             formatter: function(v, r) {
// //               var lastUpdate = v ? v.updated_at : r.updated_at;
// //               return moment(lastUpdate).format('MM/DD/YYYY');
// //             }
// //           },
// //           {
// // 	        field: 'last_event.timelineable.headline',
// //             title: 'Headline',
// //             sortable: true,
// //             searchable: false,
// //             formatter: function(v, r) {
// //
// //               return v ? v : '';
// //             }
// // 	        },{
// // 	          field: 'id',
// // 	          title: 'Actions',
// // 	          formatter: function(v, r) {
// // 	            return '<a href="admin/leads/'+v+'" class="btn-primary btn-xs glyphicon glyphicon-pencil"></a> '+
// // 	            '<button data-id="'+v+'" data-name="'+r.name+'" class="btn-danger btn-xs glyphicon glyphicon-remove row-delete"></button>';
// // 	          }
// // 					}],
// //           data: rsp.items
// //       });
//     //   $('#filters').removeClass('hide');
//     //   $('.leads-table').unblock();
//     //   $('.leads-table').bootstrapTable('uncheckAll');
//     },
//
//     function(rsp){
//       console.log('An error ocurred. Please try again later.');
//       $('.leads-table').unblock();
//     }
//   );
//
//   $('.leads-table').on('click', '.row-delete', function(e){
//     var name = $(this).data('name');
//     var id = $(this).data('id');
//     $rootScope.$broadcast('modal:delete', {name:name, cbk: function(){
//       $('.leads-table').block();
//       ApiService.lead.delete({id:id}).then(function(){
//         $('.leads-table').bootstrapTable('remove', {field: 'id', values:[id]})
//         $('.leads-table').unblock();
// 			},function(rsp){
// 				window.alert(rsp.message);
// 				$('.leads-table').unblock();
// 			});
//     }});
//   });
//
//
//
// 	$scope.selectedRows = [];
//
// 	$('.leads-table').on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function(e){
// 		$scope.selectedRows = $('.leads-table').bootstrapTable('getSelections');
// 		$scope.$digest();
// 	});
//
//   $scope.editOptions = {
//     status: [
// 			{id: 1, name: 'Attempting Contact'},
// 			{id: 3, name: 'DNC Not Interested'},
// 			{id: 4, name: 'DNC Wrong or Invalid Number'},
// 			{id: 5, name: 'Pitched'},
// 			{id: 6, name: 'New'},
// 			{id: 7, name: 'Buyer'}],
//     campaign: [],
//     agent: [],
//   }
//
// $scope.leadsEditModel = {
// 	status_id: null,
// 	campaign_id: null,
// 	user_id: null,
// };
//
//   ApiService.campaign.list().then(function(rsp){
// 		$scope.editOptions.campaign = rsp.items;
// 	});
//
//   ApiService.user.list().then(function(rsp){
// 		$scope.editOptions.agent = rsp.users;
// 	});
//
// 	$scope.modalEditShow = function() {
// 		$('#modal-leads-edit').modal('show');
// 	}
//
// 	$scope.modalMassDeleteShow = function() {
//     var name = $scope.selectedRows.length+' items';
// 		var ids = $scope.selectedRows.map(function(e,i,a){
// 			return e.id;
// 		});
//
//     $rootScope.$broadcast('modal:delete', {name:name, ng: true, cbk: function(){
//       $('.leads-table').block();
//       ApiService.lead.delete({id:ids}).then(function(){
//         $('.leads-table').bootstrapTable('remove', {field: 'id', values:ids});
//         $('.leads-table').unblock();
//       },function(rsp){
//         window.alert(rsp.message);
// 				$('.leads-table').unblock();
// 			});
//     }});
// 	}
//
// 	$scope.confirmEdit = function() {
// 		var ids = $scope.selectedRows.map(function(e,i,a){
// 			return e.id;
// 		});
//
// 		var data = {
// 			ids: ids
// 		};
//
// 		if ($scope.leadsEditModel.campaign_id) {
// 			data.campaign_id = $scope.leadsEditModel.campaign_id;
// 			var campaignName = '';
// 			for (var i = 0; i < $scope.editOptions.campaign.length; i++) {
// 				var campaign = $scope.editOptions.campaign[i];
// 				if(campaign.id === data.campaign_id) campaignName = campaign.name;
// 			}
// 		}
// 		if ($scope.leadsEditModel.user_id) data.agent_id = $scope.leadsEditModel.user_id;
// 		if ($scope.leadsEditModel.status_id) data.status_id = $scope.leadsEditModel.status_id;
//
// 		$scope.leadsEditModel = {
// 			status_id: null,
// 			campaign_id: null,
// 			user_id: null,
// 		};
//
// 		$('.leads-table').block();
// 		$('#modal-leads-edit').modal('hide');
//
// 		ApiService.lead.massEdit(data).then(function(){
// 			for (var i = 0; i < $scope.selectedRows.length; i++) {
// 				var row = $scope.selectedRows[i];
// 				if (data.campaign_id) row['campaign.name'] = campaignName;
// 				if (data.agent_id) row.user_id = data.agent_id;
// 				if (data.status_id) row.status_id = data.status_id;
// 				$('.leads-table').bootstrapTable('updateByUniqueId', {id: row.id, row: row});
// 			}
// 			$('.leads-table').unblock();
// 		},function(rsp){
// 			window.alert(rsp.message);
// 			$('.leads-table').unblock();
// 		});
// 	}

}]);
