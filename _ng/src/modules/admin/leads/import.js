angular.module('app.lead.import')
.controller('LeadImportController', ['$scope', '$q', '$window', 'ApiService', 'LeadModel', '$state', function ($scope, $q, $window, ApiService, Model, $state) {

  /* IMPORT FUNCTIONS */
  function safe_decode_range(range) {
    var o = {s:{c:0,r:0},e:{c:0,r:0}};
    var idx = 0, i = 0, cc = 0;
    var len = range.length;
    for(idx = 0; i < len; ++i) {
      if((cc=range.charCodeAt(i)-64) < 1 || cc > 26) { break; }
      idx = 26*idx + cc;
    }
    o.s.c = --idx;

    for(idx = 0; i < len; ++i) {
      if((cc=range.charCodeAt(i)-48) < 0 || cc > 9) { break; }
      idx = 10*idx + cc;
    }
    o.s.r = --idx;

    if(i === len || range.charCodeAt(++i) === 58) { o.e.c=o.s.c; o.e.r=o.s.r; return o; }

    for(idx = 0; i !== len; ++i) {
      if((cc=range.charCodeAt(i)-64) < 1 || cc > 26) { break; }
      idx = 26*idx + cc;
    }
    o.e.c = --idx;

    for(idx = 0; i !== len; ++i) {
      if((cc=range.charCodeAt(i)-48) < 0 || cc > 9) { break; }
      idx = 10*idx + cc;
    }
    o.e.r = --idx;
    return o;
  }

  function sheetToArray(sheet, opts) {

    var XLSX = $window.XLSX;

    var out = [], txt = "", qreg = /"/g;

    var o = opts == null ? {} : opts;
    if(sheet == null || sheet["!ref"] == null) { return ""; }
    var r = safe_decode_range(sheet["!ref"]);
    var FS = o.FS !== undefined ? o.FS : ",", fs = FS.charCodeAt(0);
    var RS = o.RS !== undefined ? o.RS : "\n", rs = RS.charCodeAt(0);
    var row = "", rr = "", cols = [];
    var i = 0, cc = 0, val;
    var R = 0, C = 0;
    for(C = r.s.c; C <= r.e.c; ++C) {
      cols[C] = XLSX.utils.encode_col(C);
    }
    for(R = r.s.r; R <= r.e.r; ++R) {
      row = [];
      rr = XLSX.utils.encode_row(R);
      for(C = r.s.c; C <= r.e.c; ++C) {
        val = sheet[cols[C] + rr];
        txt = val !== undefined ? ''+XLSX.utils.format_cell(val) : "";
        //console.log(txt);
  /*
        for(i = 0, cc = 0; i !== txt.length; ++i) if((cc = txt.charCodeAt(i)) === fs || cc === rs || cc === 34) {
          txt = "\"" + txt.replace(qreg, '""') + "\""; break; }
  */
        row.push(txt.replace(/^\s+|\s+$/g, ''));
      }
      out.push(row);
    }
    return out;

  }

  function parseXLS (file, callback) {
    var reader = new FileReader(file);
    var XLSX = $window.XLSX;

    reader.onload = function(e) {

      var data = e.target.result;
          var workbook = XLSX.read(data, {type : 'binary'});
          workbook.SheetNames.forEach(function(sheetName){
            var sheet = workbook.Sheets[sheetName];
            var arr = sheetToArray(sheet);
            callback(arr);
        });

    };

    reader.onerror = function(ex){
      console.log(ex);
    };
    reader.readAsBinaryString(file);
  }

  $scope.mprt = {
    dataArray: [],
    jsonArray: [],
    sampleRow: [],
    step: 1,
    firstRowAreKeys: true,
    onFileSelected: function(input) {

      var file = input.files[0];
      if(file === 'undefined') {
        return false; // rise error here
      }

      parseXLS(file, function(arr){
        if(!angular.isArray(arr)) {
            console.log('the invalid data', arr);
          $window.alert('Invalid data');
          return;
        }
        $scope.mprt.step = 2;
        $scope.mprt.dataArray = arr;
        $scope.mprt.sampleRows = $scope.mprt.getSampleRows();
        $scope.$apply();

      });


    },
    getSampleRows: function() {
      var ret = [];
      var sliceStart = $scope.mprt.firstRowAreKeys ? 1 : 0;
      var sliceEnd = $scope.mprt.dataArray.length > sliceStart + 3 ? sliceStart + 3 : $scope.mprt.dataArray.length;


      ret = $scope.mprt.dataArray.slice(sliceStart, sliceEnd);

      // if($scope.mprt.firstRowAreKeys && $scope.mprt.dataArray.length > 1) {
      //   ret = $scope.mprt.dataArray[1];
      // } else {
      //   ret = $scope.mprt.dataArray[0];
      // }

      $scope.mprt.keymap = ret[0].map(function(e,i,a){
        return $scope.mprt.keymap[i] ? $scope.mprt.keymap[i] : null;
      });

      //console.log($scope.mprt.keymap);
      return ret;
    },
    changeHeaders: function() {
      $scope.mprt.sampleRows = $scope.mprt.getSampleRows();
    },
    keymap: [
      'phone',
      'name',
      'email',
      'notes',
      'timezone'
    ],
    modelFields: [
      'phone',
      'name',
      'email',
      'notes',
      'timezone',
      'broker',
      'occupation',
      'tradesize',
      'portfolio',
      'report'
    ],
    availableModelFields:[],
    calcAvailableModelFields: function($index) {
        $scope.mprt.availableModelFields = $scope.mprt.modelFields.filter(function(e,i,a){
            return $scope.mprt.keymap.indexOf(e) === -1;
        });
    },
    showModelField: function(field) {
      var ret = ($scope.mprt.availableModelFields.indexOf(field) !== -1);
      return ret;
    },
    importEnabled: function() {
      var ret = ($scope.mprt.availableModelFields.indexOf('name') === -1) && ($scope.mprt.availableModelFields.indexOf('email') === -1) && ($scope.mprt.availableModelFields.indexOf('phone') === -1);
      return ret;
    },
    activeCampaigns: [],
    selectedCampaigns: [],
    activeAgents: [],
    selectedAgents: [],
    source: '',
    parseFields: function() {

      var startRow = $scope.mprt.firstRowAreKeys ? 1 : 0;
      //console.log('test', startRow);
      for(var r = startRow; r < $scope.mprt.dataArray.length; r++) {
        var e = $scope.mprt.dataArray[r];
        var row = {};
        for(var n=0; n< $scope.mprt.keymap.length; n++) {
          var key = $scope.mprt.keymap[n];
          if(!key) { continue; }
          var value = e[n];

          row[key] = value;
        }
        $scope.mprt.jsonArray.push(row);
      }

        var p1 = ApiService.campaign.list({filters: [{'field': 'active', 'value': 1}]});
        var p2 = ApiService.user.list({filters: [{'field': 'active', 'value': 1}, {'field': 'role_id', 'op': '>=', 'value': 2}]});
        var p3 = ApiService.source.list();
      var all = $q.all([
                            p1.then(function(rsp){
                              $scope.mprt.activeCampaigns = rsp.items;
                            }),
                            p2.then(function(rsp){
                              $scope.mprt.activeAgents = rsp.users;
                            }),
                            p3.then(function(rsp){
                              $scope.mprt.activeSources = rsp.items;
                            })
                        ])
                        .then(function(values) {
                          $scope.mprt.step = 3;

                            return values;
                        });
    },
    actionEnabled: function() {
      var ret = 	($scope.mprt.selectedAgents.length > 0) && ($scope.mprt.selectedCampaigns.length > 0);
      return ret;
    },
    actionResponse: '',
    actionError: '',
    doAction: function() {
      if ($scope.mprt.source) {
        ApiService.lead.import({
          api_token: $scope.api_token,
          campaigns: $scope.mprt.selectedCampaigns,
          agents: $scope.mprt.selectedAgents,
          leads: $scope.mprt.jsonArray,
          source: $scope.mprt.source
        }).then(function(rsp){
          $scope.mprt.actionResponse = rsp;
          $scope.leadslist.getItems();
          $state.go('admin.leads');
        },function(rsp){
          $scope.mprt.actionError = rsp;
        });
      } else {
        window.alert('Please select a source.');
      }
    }
};

  $scope.mprt.calcAvailableModelFields();

}]);
