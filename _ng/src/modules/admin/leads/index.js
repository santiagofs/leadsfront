angular.module('app.leads')
    .controller('AdminLeadsCtrl', ['$state', '$scope', '$rootScope', 'ApiService', 'ExportService', '$q', function($state, $scope, $rootScope, ApiService, ExportService, $q){
            //$state.current.data.customData1
        var fullAuxs = {
            campaigns: [],
            agents:[],
        };
        var i;

        $scope.leadslist = {
            filters: {
                source: null,
                status: null,
                timezone: null,
                campaign: null,
                agent: null,
                text: null,
            },
            auxFilters: {
                activeOnly: false
            },
            applyFilters: function() {
                $scope.leadslist.getItems();
            },
            resetFilters: function() {
                $scope.leadslist.filters = {
                    source: null,
                    status: null,
                    timezone: null,
                    campaign: null,
                    agent: null,
                    text: null,
                };
                $scope.leadslist.getItems();
            },
            filterMapping: {
                source: {field: 'leads.source_id', 'op': '=', value: null},
                status: {field: 'leads.status_id', 'op': '=', value: null},
                timezone: {field: 'leads.timezone', 'op': '=', value: null},
                campaign: {field: 'leads.campaign_id', 'op': '=', value: null},
                agent: {field: 'leads.user_id', 'op': '=', value: null},
                text: {field: 'text', 'op': 'like', value: '', skip: true},
            },
            pagination: {
                first: 1,
                last: 0,
                next: 0,
                page: 1,
                pages: 0,
                previous: 0,
                rows: 0
            },
            onPage: function(page) {
                $scope.leadslist.getItems(page);
            },
            items:[],
            selectAll: false,
            selectSome: false,
            onSelectAll: function() {
                for(i = 0; i<$scope.leadslist.items.length; i++) {
                    $scope.leadslist.items[i].selected = $scope.leadslist.selectAll;
                }
                $scope.leadslist.selectSome = $scope.leadslist.selectAll;
            },
            onSelectItem: function() {
                var a = $scope.leadslist.items.length;
                var b = $scope.leadslist.items.filter(function(e,i,a){
                    return e.selected === true;
                }).length;
                $scope.leadslist.selectAll = (a === b);
                $scope.leadslist.selectSome = !!b;
            },
            onUpdateField: function(value, field, item) {
                var deferred = $q.defer();
                var data = angular.copy(item);
                data[field] = value;
                ApiService.lead.edit({item:data}).then(
                    function(rsp) {
                        if($scope.auxsMapping[field]) {
                            var mapping = $scope.auxsMapping[field];
                            var id = item[field];
                            var list = $scope.auxs[mapping.list];
                            var selected = list.filter(function(e,i,a){
                                return e.id === id;
                            });
                            var value = selected.length ? selected[0][mapping.field] : 'Not found';
                            item[mapping.target] = value;
                        }
                        return true;
                    },
                    function(err) {
                        return false;
                    }
                );

                return deferred;
            },
            onUpdateItem: function(item) {
              var k = this.items.findIndex(function(i){return i.id === item.id;});
              if (k > -1) {
                this.items[k] = item;
              } else {
                this.items.unshift(item);
              }
            },
            sort: {field:'name', mode: '+'},
            getSortClass: function(field) {

                if($scope.leadslist.sort.field === field) {
                    if($scope.leadslist.sort.mode === '+') {
                        return 'sortUp';
                    } else {
                        return 'sortDown';
                    }
                }
                return '';
            },
            doSort: function(field) {
                $scope.leadslist.sort.mode = $scope.leadslist.sort.field === field ? ($scope.leadslist.sort.mode === '+' ? '-' : '+') : '+';
                $scope.leadslist.sort.field = field;
                $scope.leadslist.getItems();
            },
            getItems: function(page) {
                var data = {
                    page: page ? page : 1,
                    per_page: 100,
                    sort: $scope.leadslist.sort.mode+$scope.leadslist.sort.field,
                    filters: [],
                    buyers: true,
                };
                angular.forEach($scope.leadslist.filters, function(filter, field){
                    if(filter !== null) {
                        var mapped = $scope.leadslist.filterMapping[field];
                        mapped.value = angular.isObject(filter) ? filter.id : filter;

                        data.filters.push(mapped);
                    }
                });

                $('.index-content').block();
                ApiService.lead.forAdmin(data).then(
                    function(rsp) {
                        $scope.leadslist.items = rsp.items;
                        $scope.leadslist.pagination = rsp.pagination;
                        $('.index-content').unblock();
                        $scope.apply();
                    },
                    function(err) {
                        $('.index-content').unblock();
                        alert(err);
                    }
                );
            },
            export: function() {
                var data = {
                    page: 1,
                    per_page: 9999999,
                    sort: $scope.leadslist.sort.mode+$scope.leadslist.sort.field,
                    filters: [],
                    buyers: true,
                };
                angular.forEach($scope.leadslist.filters, function(filter, field){
                    if(filter !== null) {
                        var mapped = $scope.leadslist.filterMapping[field];
                        mapped.value = angular.isObject(filter) ? filter.id : filter;

                        data.filters.push(mapped);
                    }
                });

                $('.index-content').block();
                var deferred = $q.defer();
                ApiService.lead.forAdmin(data).then(
                    function(rsp) {
                        $('.index-content').unblock();
                        deferred.resolve(rsp);
                    },
                    function(err) {
                        $('.index-content').unblock();
                        alert(err);
                        deferred.reject(err);
                    }
                );

                return deferred.promise;
            },
            delete: function(item) {

              $rootScope.$broadcast('modal:delete', {name:item.name, ng: true, cbk: function(){
                $('.index-content').block();

                ApiService.lead.delete({id:[item.id]}).then(function(){
                  var k = $scope.leadslist.items.findIndex(function(i){return i.id === item.id;});
                  if (k > -1) {
                    $scope.leadslist.items.splice(k,1);
                  }
                  $('.index-content').unblock();
                },function(rsp){
                  window.alert(rsp.message);
          				$('.index-content').unblock();
          			});
              }});
            },
            modalMassDeleteShow: function() {
          		var ids = $scope.leadslist.items.reduce(function(r,e,i,a){
                if(e.selected) r.push(e.id);
          			return r;
          		}, []);
              var name = ids.length+' items';

              $rootScope.$broadcast('modal:delete', {name:name, ng: true, cbk: function(){
                $('.index-content').block();


                ApiService.lead.delete({'id[]':ids}).then(function(){
                  ids.forEach(function(id){
                    var k = $scope.leadslist.items.findIndex(function(i){return i.id === id;});
                    if (k > -1) {
                      $scope.leadslist.items.splice(k,1);
                    }
                  });
                  $('.index-content').unblock();
                },function(rsp){
                  window.alert(rsp.message);
          				$('.index-content').unblock();
          			});
              }});
          	},
            modalMassEditShow: function() {
          		$('#modal-leads-edit').modal('show');
            },
            confirmEdit: function() {
          		var ids = $scope.leadslist.items.reduce(function(r,e,i,a){
                if(e.selected) r.push(e.id);
          			return r;
          		}, []);

          		var data = {
          			ids: ids
          		};

          		if ($scope.leadsEditModel.campaign_id) data.campaign_id = $scope.leadsEditModel.campaign_id;
          		if ($scope.leadsEditModel.user_id) data.agent_id = $scope.leadsEditModel.user_id;
          		if ($scope.leadsEditModel.status_id) data.status_id = $scope.leadsEditModel.status_id;

          		$scope.leadsEditModel = {
          			status_id: '',
          			campaign_id: '',
          			user_id: '',
          		};

              $('.index-content').block();
          		$('#modal-leads-edit').modal('hide');
              ApiService.lead.massEdit(data).then(function(rsp){
                var items = rsp.items;

                items.forEach(function(item){
                    var k = $scope.leadslist.items.findIndex(function(i){return i.id === item.id;});
                    $scope.leadslist.items[k] = item;
                });

                $('.index-content').unblock();
              },function(rsp){
                window.alert(rsp.message);
        				$('.index-content').unblock();
        			});
            }
        };

        $scope.leadslist.getItems();

        $scope.auxs = {
            sources: [],
            statuses: [],
            editStatuses: [],
            timezones: [],
            campaigns: [],
            agents:[],
        };
        $scope.auxsMapping = {
            status_id: {field: 'label', list: 'statuses', target: 'status'},
            source_id: {field: 'source', list: 'sources', target: 'source'},
            user_id: {field: 'name', list: 'agents', target: 'agent'},
            campaign_id: {field: 'name', list: 'campaings', target: 'campaign'},
        };

        for(i=-12; i<=12; i++) {
            $scope.auxs.timezones.push({label:i, id: i});
        }
        $scope.auxs.editStatuses = [
            {id: 6, label: 'New'},
            {id: 1, label: 'Attempting Contact'},
            {id: 8, label: 'Pledged'},
            {id: 9, label: 'BID'},
            {id: 10, label: 'Primed'},
            {id: 5, label: 'Pitched'},
            {id: 3, label: 'DNC Not Interested'},
            {id: 11, label: 'Never Answered'},
            {id: 4, label: 'Wrong or Invalid Number'},
        ];
        $scope.auxs.statuses = [
            {id: 6, label: 'New'},
            {id: 1, label: 'Attempting Contact'},
            {id: 8, label: 'Pledged'},
            {id: 9, label: 'BID'},
            {id: 10, label: 'Primed'},
            {id: 5, label: 'Pitched'},
            {id: 7, label: 'Buyer'},
            {id: 2, label: 'Schedule Callback'},
            {id: 3, label: 'DNC Not Interested'},
            {id: 11, label: 'Never Answered'},
            {id: 4, label: 'Wrong or Invalid Number'},
        ];

    		$scope.leadsEditModel = {
    			status_id: '',
    			campaign_id: '',
    			user_id: '',
    		};

        var getAuxs = function() {
            ApiService.lead.auxs().then(
                function(rsp) {
                    $scope.auxs.agents = fullAuxs.agents = rsp.agents;
                    console.log(rsp.agents);
                    $scope.auxs.sources = rsp.sources;
                    $scope.auxs.campaigns = fullAuxs.campaigns = rsp.campaigns;

                    $scope.auxs.agents.unshift({id: null, name: '-No agent-'});
                    $scope.auxs.campaigns.unshift({id:null, name: '-No campaign-'});
                },
                function(err) {
                    alert(err);
                }
            );
        };
        getAuxs();

        $scope.showOnlyActiveItems = true;
        $scope.onChangeActiveOnly = function() {
            $scope.auxs.agents = fullAuxs.agents.filter(function(e,i,a){
                return $scope.leadslist.auxFilters.activeOnly ? Boolean(e.active) === true : true;
            });
            $scope.auxs.campaigns = fullAuxs.campaigns.filter(function(e,i,a){
                return $scope.leadslist.auxFilters.activeOnly ? Boolean(e.active) === true : true;
            });
        };

        $scope.exportLeads = function() {
            console.log('export leads');
            var headers = [['Status', 'Name', 'Phone', 'Email', 'Timezone', 'Agent', 'Source', 'Campaign', 'Modified', 'Headline', 'Last notes', 'Created']];
            // var tableData = $('.leads-table').bootstrapTable('getData');
            $scope.leadslist.export().then(
                function(rsp) {
                    var list = rsp.items.map(function(e,i,a){
                        return [e.status, e.name, e.phone, e.email, e.timezone, e.agent, e.source, e.campaign, moment(e.modified).format('MM/DD/YYYY'), e.headline, e.last_notes, moment(e.created_at).format('MM/DD/YYYY')];
                    });
                    console.log(list);
                    list = headers.concat(list);
                    ExportService.csv(list, 'leads.csv');
                }
            );

        };
    }]);
