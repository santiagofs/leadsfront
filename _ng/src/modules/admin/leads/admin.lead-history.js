angular.module('app.leads')
.controller('AdminLeadHistoryController', ['$state', '$scope', 'ApiService', 'UserService', 'ExportService', function($state, $scope, ApiService, UserService, ExportService){

  $scope.currentLead = { id: $state.params.leadId };
  $scope.leadHistory = [];

  $('.history-table').block();

  ApiService.lead.get({id: $scope.currentLead.id, relations: ['timeline.timelineable'] }).then(function(rsp){
    $scope.leadHistory = rsp.item.timeline;

    $('.history-table').unblock();
  });
}]);
