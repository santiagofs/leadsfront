angular.module('app.leads')
.controller('AdminLeadSoldController', ['$state', '$scope', 'ApiService', 'UserService', 'ExportService', function($state, $scope, ApiService, UserService, ExportService){

  $scope.lead = { id: $state.params.leadId };
  $scope.leadHistory = [];

  $('.table.lead-sold').block();

  ApiService.lead.get({id: $scope.lead.id, relations: ['sales.timelineable'] }).then(function(rsp){
    $scope.leadHistory = rsp.item.sales;
	console.log('list items', rsp.item)
    $('.table.lead-sold').unblock();
  });
}]);
