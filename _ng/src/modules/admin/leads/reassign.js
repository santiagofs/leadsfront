angular.module('app.lead.reassign')
.controller('LeadReassignController', ['$scope', '$q', '$window', 'ApiService', 'LeadModel', '$state', function ($scope, $q, $window, ApiService, Model, $state) {

  $('.reassign-form').block();

  var p1 = ApiService.campaign.list({api_token: $scope.api_token,filters: [{'field': 'active', 'value': 1}]});
  var p2 = ApiService.user.list({api_token: $scope.api_token, filters: [{'field': 'active', 'value': 1}, {'field': 'role_id', 'value': 4}]});
  var all = $q.all([
    p1.then(function(rsp){
	    
      	$scope.reassign.activeCampaigns = rsp.items;
       $scope.reassign.activeCampaigns.unshift({id: 0, name: '[no campaign assigned]'});
    }),
    p2.then(function(rsp){
      $scope.reassign.activeAgents = rsp.users;
    })
  ])
  .then(function(values) {
    $scope.reassign.step = 3;
    $('.reassign-form').unblock();

    return values;
  });

  $scope.reassign = {
    activeCampaigns: [],
    selectedCampaigns: [],
    activeAgents: [],
    selectedAgents: [],
    actionEnabled: function() {
      var ret = 	($scope.reassign.selectedAgents.length > 0) && ($scope.reassign.selectedCampaigns.length > 0);
      return ret;
    },
    actionResponse: '',
    actionError: '',
    doAction: function() {
      ApiService.lead.reassign({
        api_token: $scope.api_token,
        campaigns: $scope.reassign.selectedCampaigns,
        agents: $scope.reassign.selectedAgents,
      }).then(function(rsp){
        $scope.reassign.actionResponse = rsp;
        $state.go('admin.leads');
      },function(rsp){
        $scope.reassign.actionError = rsp;
      });
    }
  }

}]);
