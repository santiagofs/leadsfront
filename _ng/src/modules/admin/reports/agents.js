angular.module('app.reports')
.controller('ReportAgentsController', ['$scope', 'ApiService', '$window', 'ExportService', function($scope, ApiService, $window, ExportService){

	$scope.query = {
		start_date: moment().add(-30,'days').format('Y-M-D hh:mm:ss'),
		end_date: moment().format('Y-M-D hh:mm:ss')
	}

  $('.datepicker').datetimepicker({
    format: 'YYYY-MM-DD hh:mm:ss'
  }).on('dp.change', function(e){
    var $dp = $(e.currentTarget);
    $scope.query[$dp.attr('name')] = $dp.val();
  });

	$scope.getReport = function(){
		var start = $scope.query.start_date ? $scope.query.start_date : moment().add(-30,'days').format('Y-M-D hh:mm:ss');
		var end = $scope.query.end_date ? $scope.query.end_date : moment().format('Y-M-D hh:mm:ss');

		ApiService.reports.agentTotals({
			per_page: 200,
			'start': start,
			'end': end,
		}).then(function(rsp){
			var chartData = rsp.sales.reverse();

			$window.AmCharts.makeChart( "agents-chart", {
		    "type": "serial",
		    "theme": "light",
		    "fontFamily": "Roboto",
		    "dataProvider": chartData,
		    "columnSpacing": 0,
		    "columnWidth": 0.8,
		    "balloon": {
		      "textAlign": "left",
		    },
		    "graphs": [{
		      "fillAlphas": 0.8,
		      "lineAlpha": 1,
		      "type": "column",
		      "valueField": "gt",
		      "balloonText": "<b>$[[total]]</b>",
		      "lineColor": '#ffb739'
		    }],
		    "categoryField": "user",
		    "chartCursor": {
						"cursorAlpha": 0.3,
						"cursorColor": '#000',
						"zoomable": false
		    },
		    "valueAxes": [{
		      "minimum": 0
		      //"labelFunction": function(value){return value+'%'}
		    }]
		  });
		});


		ApiService.reports.all({
			filters: [{
				field: 'sales.created_at',
				value: start,
				op: '>='
			}, {
				field: 'sales.created_at',
				value: end,
				op: '<='
			}],
			select: 'sales.created_at,total,amount',
			sort: '-sales.created_at',
			per_page: 99999
		}).then(function(rsp){

			//console.log(rsp);
			var salesByDay = {};
			rsp.sales.forEach(function(r){
				var day = r.created_at.split(' ')[0];
				if(!salesByDay[day]) salesByDay[day] = { day: day };

				if(!salesByDay[day].total) salesByDay[day].total = 0;
				salesByDay[day].total += parseFloat(r.total);

				if(!salesByDay[day].amount) salesByDay[day].amount = 0;
				salesByDay[day].amount += parseInt(r.amount);
			});

			var dataArr = [];
			for (var day in salesByDay) {
				dataArr.push({
					date: day,
					total: salesByDay[day].total.toFixed(2),
					amount: salesByDay[day].amount
				});
			}

			if ($('.daily-table').html()) {
				$('.daily-table').bootstrapTable('load', dataArr)
			} else {
				$('.daily-table').bootstrapTable({
					pagination: true,
					pageSize: 15,
					columns: [{
						field: 'date',
						title: 'Date',
						sortable: true,
						formatter: function(v){
							return moment(v).format('MM/DD/YYYY');
						}
					},{
						field: 'amount',
						title: 'Sales',
						align: 'right',
						sortable: true,
					},{
						field: 'total',
						title: 'Value',
						align: 'right',
						sortable: true,
						formatter: function(v) {
							return '$'+v
						}
					}],
					data: dataArr
				});
			}

			var chartData = dataArr.reverse();

			$window.AmCharts.makeChart( "daily-chart", {
		      "type": "serial",
		      "fontFamily": 'Roboto',
		      "marginRight":30,
		      "legend": {
							"enabled":false
		      },
		      "dataProvider": chartData,
		      "valueAxes": [{
		          "stackType": "regular",
		          "gridAlpha": 0.07,
		          "position": "left"
		      }],
		      "graphs": [{
		        "balloonText": "<b>$[[total]]</b>",
		        "fillAlphas": 0.8,
		        "lineAlpha": 1,
		        "lineThickness": 1,
		        "lineColor": '#58cfd4',
		        "visibleInLegend": false,
		        "valueField": 'total'
		      }],
		      "plotAreaBorderAlpha": 0,
		      "marginTop": 10,
		      "marginLeft": 0,
		      "marginBottom": 0,
		      "chartCursor": {
		          "cursorAlpha": 0.3,
							"cursorColor": '#000',
							"zoomable": false
		      },
		      "dataDateFormat": "YYYY-MM-DD",
		      "categoryField": "date",
		      "categoryAxis": {
		          "dateFormats": [{
		              "period": "DD",
		              "format": "DD/MM"
		          }, {
		              "period": "WW",
		              "format": "DD/MM"
		          }, {
		              "period": "MM",
		              "format": "DD/MM"
		          }, {
		              "period": "YYYY",
		              "format": "YYYY"
		          }],
		          "parseDates": true,
		          "axisColor": "#555555",
		          "gridAlpha": 0.1,
		          "gridColor": "#FFFFFF",
		      }
		  });

			//console.log(chartData);
		},console.log);
	};

	$scope.getReport();

	$scope.exportTable = function() {
		console.log('export sales');
		var headers = ['Date', 'Sales', 'Value'];
    var tableData = $('.daily-table').bootstrapTable('getData');
		var list = tableData.map(function(e,i,a){
			return [moment(e.date).format('MM/DD/YYYY'), e.amount.toString(), e.total];
		});

		ExportService.csv(list, 'sales.csv');
	}
}]);
