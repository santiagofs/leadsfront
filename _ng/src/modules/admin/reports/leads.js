angular.module('app.reports')
.controller('ReportLeadsController', ['$scope', 'ApiService', '$window', 'ExportService', function($scope, ApiService, $window, ExportService){

	$scope.rpt = {
		active: true,
		grouping: 'user_id'
	}
	function getReport() {
		ApiService.reports.leads({'group_by' : $scope.rpt.grouping, 'only_active': $scope.rpt.active }).then(function(rsp){

			if ($('.leads-table').html()) {
				$('.leads-table').bootstrapTable('load', rsp.sales)
			} else {
				$('.leads-table').bootstrapTable({
					pagination: true,
					pageSize: 15,
					columns: [{
						field: '0',
						title: '&nbsp;',
						sortable: true,
						cellStyle: function() {
						  return { css: { 'min-width': '150px' } };
						}
					},{
						field: '1',
						title: 'T',
						align: 'right',
						sortable: true,
					},{
						field: '2',
						title: 'N',
						align: 'right',
						sortable: true,
					},{
						field: '3',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					},{
						field: '4',
						title: 'AC',
						align: 'right',
						sortable: true,
					},{
						field: '5',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					},{
						field: '6',
						title: 'P',
						align: 'right',
						sortable: true,
					},{
						field: '7',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					},{
						field: '8',
						title: 'CB',
						align: 'right',
						sortable: true,
					},{
						field: '9',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					},{
						field: '10',
						title: 'NI',
						align: 'right',
						sortable: true,
					},{
						field: '11',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					},{
						field: '12',
						title: 'WN',
						align: 'right',
						sortable: true,
					},{
						field: '13',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					},{
						field: '14',
						title: 'PL',
						align: 'right',
						sortable: true,
					},{
						field: '15',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					},{
						field: '16',
						title: 'BID',
						align: 'right',
						sortable: true,
					},{
						field: '17',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					},{
						field: '18',
						title: 'Pr',
						align: 'right',
						sortable: true,
					},{
						field: '19',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					},{
						field: '20',
						title: 'NA',
						align: 'right',
						sortable: true,
					},{
						field: '21',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					},{
						field: '22',
						title: 'O',
						align: 'right',
						sortable: true,
					},{
						field: '21',
						title: '&nbsp;',
						align: 'right',
						formatter: function(v){ return v+'%'; }
					}],
					data: rsp.sales
				});

		    $('#filters').removeClass('hide');
			}
		});
	}

	$scope.grActive = function(grouper){
		return grouper === $scope.rpt.grouping ? 'active' : '';
	};
	$scope.grChange = function(grouper){
		$scope.rpt.grouping = grouper;
		getReport();
	};

	$scope.requery = function() {
		getReport();
	};

	getReport();

}]);
