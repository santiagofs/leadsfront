angular.module('app.notifications')
.controller('AdminNotificationsController', ['$scope', '$rootScope', 'ApiService', 'UserService', function($scope, $rootScope, ApiService, UserService){

  $('.notifications-table').block();

  ApiService.notification.list().then(function(rsp){
    $('.notifications-table').bootstrapTable({
      rowStyle: function(r){
        return {
          classes: (!r.status || !r.status.read) && UserService.current().role !== 'admin' ? 'info' : ''
        }
      },
      columns: [{
      //   checkbox: true
      // },{
        field: 'subject',
        title: 'Subject',
        sortable: true,
        formatter: function(v, r){
          return '<button class="btn btn-link" data-toggle="modal" data-target="#notificationModal" data-notification-id="'+r.id+'">'+v+'</button>'
        }
      },{
        field: 'updated_at',
        title: 'Date',
        sortable: true,
        formatter: function(v) {
          return moment(v).format('DD MMM YYYY');
        }
      // },{
      //   field: 'id',
      //   title: 'Actions',
      //   formatter: function(v, r) {
      //     return '<button data-id="'+v+'" data-name="'+r.subject+'" class="btn-danger btn-xs glyphicon glyphicon-remove row-delete"></button>';
      //   }
      }],
      data: rsp.items
    });

    $('#filters').removeClass('hide');
    $('.notifications-table').unblock();
    $scope.notifications = rsp.items;
  }, function(rsp){
    $('.notifications-table').unblock();
    console.log(rsp);
  });


  $('.notifications-table').on('click', '.row-delete', function(e){
    var name = $(this).data('name');
    var id = $(this).data('id');
    $rootScope.$broadcast('modal:delete', {name:name, cbk: function(){
      $('.notifications-table').block();
      ApiService.notification.delete({id:id}).then(function(){
        $('.notifications-table').bootstrapTable('remove', {field: 'id', values:[id]})
        $('.notifications-table').unblock();
      },function(rsp){
        window.alert(rsp.message);
				$('.notifications-table').unblock();
			});
    }});
  });

  $scope.currNotification = null;
  $('#notificationModal').on('show.bs.modal', function(e){
    var currNotificationId = $(e.relatedTarget).data('notification-id');
    $(e.relatedTarget).closest('tr').removeClass('info');
    $(e.relatedTarget).closest('tr').find('.notification-read').remove();
    $scope.currNotification = $.grep($scope.notifications, function(r){ return r.id === currNotificationId; })[0];

    $scope.currNotification.date = moment($scope.currNotification.updated_at).format('hh:mm MM/DD/YYYY')

    ApiService.notification.read({id:currNotificationId})

    $scope.$apply();
  });

  $('.notifications-table').on('click', '.notification-read', function(e){
    var currNotificationId = $(e.currentTarget).data('notification-id');
    $(e.currentTarget).closest('tr').removeClass('info');
    $(e.currentTarget).remove();
    ApiService.notification.read({id:currNotificationId});
  });

  $scope.selectedRows = [];

	$('.notifications-table').on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function(e){
		$scope.selectedRows = $('.notifications-table').bootstrapTable('getSelections');
		$scope.$digest();
	});

	$scope.modalMassDeleteShow = function() {
    var name = $scope.selectedRows.length+' items';
		var ids = $scope.selectedRows.map(function(e,i,a){
			return e.id;
		});

    $rootScope.$broadcast('modal:delete', {name:name, ng: true, cbk: function(){
      $('.notifications-table').block();
      ApiService.notification.delete({id:ids}).then(function(){
        $('.notifications-table').bootstrapTable('remove', {field: 'id', values:ids});
        $('.notifications-table').unblock();
      },function(rsp){
        window.alert(rsp.message);
				$('.notifications-table').unblock();
			});
    }});
	};

}]);
