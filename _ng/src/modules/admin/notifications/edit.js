angular.module('app.notification.edit')
.controller('NotificationEditController', ['$state', '$scope', 'ApiService', 'NotificationModel', function($state, $scope, ApiService, Model){

  var notificationId = $state.params.notificationId === 'new' ? null : $state.params.notificationId;


  if(notificationId) {
    $('form').block();
    ApiService.notification.get({ id : notificationId }).then(function(rsp){
      $scope.notification = rsp.item;
      $('form').unblock();
    },function(rsp){
      $scope.notification = Model.empty();
      $('form').unblock();
    })
  } else {
    $scope.notification = Model.empty();
  }

  $scope.saveCampaign = function(){
    ApiService.notification.edit({ item: $scope.notification }).then(function(rsp){
      $state.go('admin.notifications');
    },function(rsp){
      console.log(rsp);
    });
  }
}]);
