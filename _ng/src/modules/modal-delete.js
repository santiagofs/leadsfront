angular.module('app.modal.delete').controller('ModalDeleteCtrl', ['$scope', function($scope){

  $scope.deletable = {
    name: '',
    cbk: function(){}
  };

  $scope.$on('modal:delete', function(e, opt){
    console.log(opt);
    $scope.deletable.name = opt.name;
    $scope.deletable.cbk = function(){
      opt.cbk();
      $('#modal-delete').modal('hide');
    };

    if(!opt.ng) $scope.$digest();

    $('#modal-delete').modal('show');
  });
}]);
