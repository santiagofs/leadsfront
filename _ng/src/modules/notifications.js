angular.module('app.notifications')
.controller('NotificationsController', ['$scope', 'ApiService', 'UserService', function($scope, ApiService, UserService){

  $('.notifications-table').block();

  ApiService.notification.list().then(function(rsp){
    $('.notifications-table').bootstrapTable({
      rowStyle: function(r){
        return {
          classes: (!r.status || !r.status.read) && UserService.current().role !== 'admin' ? 'info' : ''
        }
      },
      columns: [{
        field: 'subject',
        title: 'Subject',
        sortable: true,
        formatter: function(v, r){
          return '<button class="btn btn-link" data-toggle="modal" data-target="#notificationModal" data-notification-id="'+r.id+'">'+v+'</button>'
        }
      },{
        field: 'updated_at',
        title: 'Date',
        sortable: true,
        formatter: function(v) {
          return moment(v).format('DD MMM YYYY');
        }
      },{
        field: 'id',
        title: 'Actions',
        visible: UserService.current().role !== 'admin',
        formatter: function(v, r) {
          return !r.status || !r.status.read ? '<button class="btn btn-default btn-xs notification-read" data-notification-id="'+v+'">Mark as Read</button>' : '';
        }
      }],
      data: rsp.items
    });
    $('.notifications-table').unblock();
    $scope.notifications = rsp.items;
  }, function(rsp){
    $('.notifications-table').unblock();
    console.log(rsp);
  });

  $scope.currNotification = null;
  $('#notificationModal').on('show.bs.modal', function(e){
    var currNotificationId = $(e.relatedTarget).data('notification-id');
    $(e.relatedTarget).closest('tr').removeClass('info');
    $(e.relatedTarget).closest('tr').find('.notification-read').remove();
    $scope.currNotification = $.grep($scope.notifications, function(r){ return r.id === currNotificationId; })[0];

    $scope.currNotification.date = moment($scope.currNotification.updated_at).format('hh:mm MM/DD/YYYY')

    ApiService.notification.read({id:currNotificationId})

    $scope.$apply();
  });

  $('.notifications-table').on('click', '.notification-read', function(e){
    var currNotificationId = $(e.currentTarget).data('notification-id');
    $(e.currentTarget).closest('tr').removeClass('info');
    $(e.currentTarget).remove();
    ApiService.notification.read({id:currNotificationId});
  });

}]);
