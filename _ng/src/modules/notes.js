angular.module('app.notes')
.controller('NotesController', ['$scope', 'ApiService', function($scope, ApiService){

  $('.notes-table').block();

  ApiService.note.list().then(function(rsp){
    $('.notes-table').bootstrapTable({
      columns: [{
        field: 'subject',
        title: 'Subject',
        sortable: true
      }, {
        field: 'body',
        title: 'Body',
        sortable: true
      }, {
        field: 'created_at',
        title: 'Date',
        sortable: true
      }],
      data: rsp.items
    });

    $('.notes-table').unblock();
  }, function(rsp){
    console.log(rsp);
    $('.notes-table').unblock();
  });

}]);
