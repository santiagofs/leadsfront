angular.module('app.leads')
	.controller('headerUserCtrl', ['$scope', '$interval', 'ApiService', 'UserService', function($scope, $interval, ApiService, UserService){
		$scope.notificationCount = 0;
		$scope.notificationsLink = $scope.user.role +'.notifications'; //$scope.user.role_id == 2 ? 'admin/notifications'

		function getNotifications() {
			if(UserService.isLogged() && $scope.user.role !== 'admin'){
				ApiService.notification.unread().then(function(rsp){
					$scope.notificationCount = rsp.notifications.length;
				})
			}
		}
		$interval(getNotifications, 5*60*1000);
		getNotifications();
	}]);
