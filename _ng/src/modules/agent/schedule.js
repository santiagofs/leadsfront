angular.module('app.schedule')
	.controller('ScheduleController',
		['$scope', 'ApiService', 'UserService', 'SaleModel', 'ScheduleModel', 'NoteModel', 'TimelineEventModel', 'LeadsList',
		function($scope, ApiService, UserService, SaleModel, ScheduleModel, NoteModel, TimelineEventModel, LeadsList){

			ApiService.campaign.list({
				filters: [
					{field: 'active', value: 1}
				]
			}).then(function(rsp){
				$scope.campaigns = rsp.items;
			});

			$scope.filterOptions = {
		    source: [],
		    status: [],
		    timezone: [],
		  };

		  $scope.filter = {
		    'status.name': null,
		    timezone: null,
		    source: null,
		  };

		  $scope.makeFilter = function(){
		    var completeFilter = {};
		    $scope.filtersCleaneable = false;

		    for (var prop in $scope.filter) {
		      if ($scope.filter[prop]) {
		        $scope.filtersCleaneable = true;
		        completeFilter[prop] = $scope.filter[prop];
		      }
		    }

				LeadsList.filterBy(completeFilter);
		  };

		  $scope.cleanFilters = function(){
		    for (var prop in $scope.filter) {
		      $scope.filter[prop] = null;
		    }

		    $scope.makeFilter();
		  };

			function collectFilters() {
				for (var i = 0; i < $scope.leadsList.length; i++) {
					var lead = $scope.leadsList[i];

					if(lead.source) {
						if ($scope.filterOptions.source.indexOf(lead.source.source) < 0) {
							$scope.filterOptions.source.push(lead.source.source);
						}
					}
					if ($scope.filterOptions.status.indexOf(lead.status.name) < 0) {
						$scope.filterOptions.status.push(lead.status.name);
					}
					if ($scope.filterOptions.timezone.indexOf(lead.timezone) < 0) {
						$scope.filterOptions.timezone.push(lead.timezone);
					}
				}
			}

			var query = {
				filters: [{ field: 'status_id', value: '2' }],
				per_page: 99999
			};

			LeadsList.init('.leads-table');
			LeadsList.block(true);
			ApiService.timeline.list(query).then(
				function(rsp){
					$scope.leadsList = rsp.items;
					collectFilters();
					LeadsList.render(rsp.items);
					LeadsList.block(false);
				},
				function(rsp){
					console.log('An error ocurred. Please try again later.');
					$('.leads-table').unblock();
				}
			);

		    var initEvents = function() {
				$scope.leadHistory = [];
				$scope.soldHistory = [];
			}
			var currLeadId = null;

			$scope.updateTableData = function(leadId) {
				$scope.currLead = LeadsList.updateByLeadId(leadId);
			};

			$scope.getLeadHistory = function(leadId) {
				ApiService.lead.get({id: leadId, relations: ['sales.timelineable'] }).then(function(rsp){
					$scope.soldHistory = rsp.item.sales;
				});
				ApiService.lead.get({id: leadId, relations: ['timeline.timelineable'] }).then(function(rsp){
					$scope.leadHistory = rsp.item.timeline;
				});
			};

			$(document).on('show.bs.modal', '#leadModal', function(e){

				currLeadId = $(e.relatedTarget).data('id');

				$scope.currLead = LeadsList.getById(currLeadId);
				initEvents();

				ApiService.lead.get({id: currLeadId, relations: ['sales.timelineable'] }).then(function(rsp){
					$scope.soldHistory = rsp.item.sales;
				});
				ApiService.lead.get({id: currLeadId, relations: ['timeline.timelineable'] }).then(function(rsp){
					$scope.leadHistory = rsp.item.timeline;
				});
			});

		}]);
