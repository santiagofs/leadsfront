angular.module('app.leads')
.controller('LeadsController', ['$scope', 'ApiService', 'UserService', 'ExportService', 'SaleModel', 'ScheduleModel', 'NoteModel', 'TimelineEventModel', 'LeadsList',
	function($scope, ApiService, UserService, ExportService, SaleModel, ScheduleModel, NoteModel, TimelineEventModel, LeadsList){

	ApiService.campaign.list({
		filters: [
			{field: 'active', value: 1}
		]
	}).then(function(rsp){
		$scope.campaigns = rsp.items;
	});

  $scope.filterOptions = {
    source: [],
    status: ['New', 'Attempting Contact', 'Pitched', 'Schedule Callback', 'BID', 'Pledged', 'Primed', 'Never Answered'],
    timezone: [],
  };

	for (var i = -12; i < 13; i++) {
		$scope.filterOptions.timezone.push(i.toString());
	}

  $scope.filter = {
    'status.name': null,
    timezone: null,
    source: null,
  };

  $scope.makeFilter = function(){
    var completeFilter = {};
    $scope.filtersCleaneable = false;

    for (var prop in $scope.filter) {
      if ($scope.filter[prop]) {
        $scope.filtersCleaneable = true;
        completeFilter[prop] = $scope.filter[prop];
      }
    }

		LeadsList.filterBy(completeFilter);
  };

  $scope.cleanFilters = function(){
    for (var prop in $scope.filter) {
      $scope.filter[prop] = null;
    }

    $scope.makeFilter();
  };

	function collectFilters() {
		for (var i = 0; i < $scope.leadsList.length; i++) {
			var lead = $scope.leadsList[i];

			if(lead.source) {
				if ($scope.filterOptions.source.indexOf(lead.source.source) < 0) {
					$scope.filterOptions.source.push(lead.source.source);
				}
			}
		}
	}

	LeadsList.init('.leads-table');

	LeadsList.block(true);
	ApiService.timeline.list({
		per_page: 99999,
		filters: [
			{field:'type_id', op: '!=', value: 2}
		]
	}).then(
		function(rsp){

			$scope.leadsList = rsp.items;
			collectFilters();
			LeadsList.render(rsp.items);
			LeadsList.block(false);
		},
		function(rsp){
			console.log('An error ocurred. Please try again later.');
			$('.leads-table').unblock();
		}
	);

    var initEvents = function() {
		$scope.leadHistory = [];
		$scope.soldHistory = [];
	}
	var currLeadId = null;

	$scope.updateTableData = function(leadId) {
		var nextLead = LeadsList.updateByLeadId(leadId)
		if (nextLead) {
			$scope.currLead = nextLead;
		} else {
			setTimeout(function(){
				$('#leadModal').modal('hide')
			}, 1);
			alert("You've reached the end of the list");
		}
	}

	$scope.showEdit = function() {
		$('#leadModal').modal('show');
	};

	$scope.getLeadHistory = function(leadId) {
		ApiService.lead.get({id: leadId, relations: ['sales.timelineable'] }).then(function(rsp){
			$scope.soldHistory = rsp.item.sales;
		});
		ApiService.lead.get({id: leadId, relations: ['timeline.timelineable'] }).then(function(rsp){
			$scope.leadHistory = rsp.item.timeline;
		});
	};

	$(document).on('show.bs.modal', '#leadModal', function(e){

		currLeadId = $(e.relatedTarget).data('id');

		$scope.currLead = LeadsList.getById(currLeadId);
		initEvents();
		$scope.getLeadHistory(currLeadId);
	});


	$(document).on('hide.bs.modal', '#leadModal', function(e){
		$scope.currLead = {};
		$scope.$digest();
	});


}]);
