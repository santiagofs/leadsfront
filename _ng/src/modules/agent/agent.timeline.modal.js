angular.module('app.leads')
	.controller('AgentTimelineModalCtrl',
		['$scope', 'ApiService', 'UserService', 'SaleModel', 'ScheduleModel', 'NoteModel', 'TimelineEventModel', 'LeadsList',
		function($scope, ApiService, UserService, SaleModel, ScheduleModel, NoteModel, TimelineEventModel, LeadsList){

			$scope.ui = {
				selectedForm: 'event'
			};
			$scope.forms = {};

			var init = function (user) {
				console.log('user >>> ', user);
				if(!user || !user.id) return;

				$scope.getLeadHistory(user.id);
				$scope.schedule = ScheduleModel.empty();
				$scope.schedule.call_date = moment().utcOffset(parseInt(user.timezone)).add(1, 'hour').minutes(0).seconds(0);

				$scope.sale = SaleModel.empty();

				$scope.availableCampaigns = angular.copy($scope.campaigns);
				if(user.campaign_id && user.type_id !==2) {
					$scope.sale.campaign_id = user.campaign_id;
					var test = $scope.campaigns.filter(function(e,i,a){
						return e.id === user.campaign_id;
					});
					if(test.length) $scope.availableCampaigns = test;
				}

				$scope.event = TimelineEventModel.empty();
				$scope.note = NoteModel.empty();
				$scope.selectedForm = '';


				if($scope.forms.eventForm) {
					$scope.forms.eventForm.$setPristine();
				}
				
				

			}

			$scope.$watch(function () {
				return $scope.currLead;
			}, function (newValue, oldValue) {
				if (newValue !== oldValue) {
				   init($scope.currLead);
				}
			}, true);

			$scope.saveEvent = function(){

				var data = { item: $scope[$scope.selectedForm] };
				data.item.lead_id = $scope.currLead.id;
				data.item.user_id = UserService.current().id;

				if(data.item.call_date && moment.isMoment(data.item.call_date)) {
					var date = data.item.call_date.utc();
	                data.item.call_date = date.format('YYYY-MM-DD HH:mm:ss');
				}

				ApiService[$scope.selectedForm].edit(data).then(function(rsp){

					//console.log($scope.updateTableData);
					if($scope.updateTableData) {
						$scope.updateTableData(data.item.lead_id);
					}

					$scope[$scope.selectedForm] = {};
					init();

				}, function(rsp){
					console.log(rsp);
				});
			};

		}]);
