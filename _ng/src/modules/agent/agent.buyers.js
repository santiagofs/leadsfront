angular.module('app.buyers')
.controller('BuyersController',
	['$scope', 'ApiService', 'UserService', 'SaleModel', 'ScheduleModel', 'NoteModel', 'TimelineEventModel', 'LeadsList',
	function($scope, ApiService, UserService, SaleModel, ScheduleModel, NoteModel, TimelineEventModel, LeadsList){

	ApiService.campaign.list({
		filters: [
			{field: 'active', value: 1}
		]
	}).then(function(rsp){
		$scope.campaigns = rsp.items;
	})

  $scope.filterOptions = {
    source: [],
    status: ['Buyer', 'Attempting Contact', 'Pitched', 'Schedule Callback'],
    timezone: [],
  };

	for (var i = -12; i < 13; i++) {
		$scope.filterOptions.timezone.push(i.toString());
	}

  $scope.filter = {
    'status.name': null,
    timezone: null,
    source: null,
  };

  $scope.makeFilter = function(){
    var completeFilter = {};
    $scope.filtersCleaneable = false;

    for (var prop in $scope.filter) {
      if ($scope.filter[prop]) {
        $scope.filtersCleaneable = true;
        completeFilter[prop] = $scope.filter[prop];
      }
    }

		LeadsList.filterBy(completeFilter);
  };

  $scope.cleanFilters = function(){
    for (var prop in $scope.filter) {
      $scope.filter[prop] = null;
    }

    $scope.makeFilter();
  };

	function collectFilters() {
		for (var i = 0; i < $scope.leadsList.length; i++) {
			var lead = $scope.leadsList[i];

			if(lead.source) {
				if ($scope.filterOptions.source.indexOf(lead.source.source) < 0) {
					$scope.filterOptions.source.push(lead.source.source);
				}
			}
		}
	}

	var query = {
		filters: [{ field: 'type_id', value: '2' }],
		per_page: 99999
	};

	LeadsList.init('.leads-table');
	LeadsList.block(true);
	ApiService.timeline.list(query).then(
		function(rsp){
			$scope.leadsList = rsp.items;
			collectFilters();
			LeadsList.render(rsp.items);
			LeadsList.block(false);
		},
		function(rsp){
			console.log('An error ocurred. Please try again later.');
			LeadsList.block(false);
		}
	);

	var initEvents = function() {
		$scope.schedule = ScheduleModel.empty();
		$scope.schedule.call_date = moment().add(1, 'hour').format('YYYY-MM-DD HH:00');
		$scope.sale = SaleModel.empty();
		$scope.event = TimelineEventModel.empty();
		$scope.note = NoteModel.empty();

		$scope.leadHistory = [];
		$scope.soldHistory = [];

	}
	var currLeadId = null;

	$scope.updateTableData = function(leadId) {
		var nextLead = LeadsList.updateByLeadId(leadId);
		if (nextLead) {
			$scope.currLead = nextLead
		} else {
			setTimeout(function(){
				$('#leadModal').modal('hide')
			}, 1);
			alert("You've reached the end of the list");
		}
	};

	$scope.getLeadHistory = function(leadId) {
		ApiService.lead.get({id: leadId, relations: ['sales.timelineable'] }).then(function(rsp){
			$scope.soldHistory = rsp.item.sales;
		});
		ApiService.lead.get({id: leadId, relations: ['timeline.timelineable'] }).then(function(rsp){
			$scope.leadHistory = rsp.item.timeline;
		});
	};

	$(document).on('show.bs.modal', '#leadModal', function(e){

		currLeadId = $(e.relatedTarget).data('id');

		$scope.currLead = LeadsList.getById(currLeadId);
		$scope.selectedForm = 'sale';
		initEvents();

		ApiService.lead.get({id: currLeadId, relations: ['sales.timelineable'] }).then(function(rsp){
			$scope.soldHistory = rsp.item.sales;
		});
		ApiService.lead.get({id: currLeadId, relations: ['timeline.timelineable'] }).then(function(rsp){
			$scope.leadHistory = rsp.item.timeline;
		});
	});

	$scope.currForm = 'event';

	function updateRow(i){
		ApiService.lead.list({ filters: [{ field: 'id', value: $scope.currLead.id}], relations: ['status', 'lastEvent.timelineable'] }).then(function(rsp){
		$('.leads-table').bootstrapTable('updateRow', {index: i, row: rsp.items[0]})
		}, function(rsp){
			console.log(rsp);
		});
	}

}]);
