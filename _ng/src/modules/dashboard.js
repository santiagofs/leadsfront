angular.module('app')
.controller('dashboardCtrl', ['$scope', 'ApiService', '$window', 'UserService', function($scope, ApiService, $window, UserService){

ApiService.campaign.list({
	sort: '-created_at',
	per_page: 5
}).then(function(rsp){
	$scope.lastCampaigns = rsp.items;
},console.log);

var user = UserService.current();
var lastSalesQuery = {
	sort: '-sales.created_at',
	per_page: 5
};

if (user.role !== 'admin') {
	lastSalesQuery.filters =[{
		field: 'sales.user_id',
		value: user.id
	}]
}

ApiService.reports.all(lastSalesQuery).then(function(rsp){
	$scope.lastSales = rsp.sales;
},console.log);

ApiService.reports.agentTotals({
	per_page: 200,
	'start': moment().add(-7,'days').format('Y-M-D hh:mm:ss'),
	'end': moment().format('Y-M-D hh:mm:ss'),
}).then(function(rsp){
	var chartData = rsp.sales.reverse();

	$window.AmCharts.makeChart( "agents-chart", {
    "type": "serial",
    "theme": "light",
    "fontFamily": "Roboto",
    "dataProvider": chartData,
    "columnSpacing": 0,
    "columnWidth": 0.8,
    "balloon": {
      "textAlign": "left",
    },
    "graphs": [{
      "fillAlphas": 0.8,
      "lineAlpha": 1,
      "type": "column",
      "valueField": "gt",
      "balloonText": "<b>$[[total]]</b>",
      "lineColor": '#ffb739'
    }],
    "categoryField": "user",
    "chartCursor": {
				"cursorAlpha": 0.3,
				"cursorColor": '#000',
				"zoomable": false
    },
    "valueAxes": [{
      "minimum": 0
      //"labelFunction": function(value){return value+'%'}
    }]
  });
});

var dailyQuery = {
	filters: [{
		field: 'sales.created_at',
		value: moment().add(-7, 'days').format('YYYY-MM-DD hh:mm:ss'),
		op: '>'
	}],
	select: 'sales.created_at,total',
	sort: '-sales.created_at',
	per_page: 99999
};

if (user.role !== 'admin') {
	dailyQuery.filters.push({
		field: 'sales.user_id',
		value: user.id
	})
}

ApiService.reports.all(dailyQuery).then(function(rsp){

	//console.log(rsp);
	var totalSalesByDay = {};
	var totalValue = 0;
	rsp.sales.forEach(function(r){
		var day = r.created_at.split(' ')[0];
		if(!totalSalesByDay[day]) totalSalesByDay[day] = 0;
		totalSalesByDay[day] += parseFloat(r.total);
		totalValue += parseFloat(r.total);
	});

	$scope.totalSales = rsp.sales.length;
	$scope.totalValue = totalValue;
	$scope.avgSaleValue = totalValue / rsp.sales.length;

	var chartData = [];
	for (var day in totalSalesByDay) {
		chartData.push({
			day: day,
			value: totalSalesByDay[day].toFixed(2)
		})
	}

	chartData = chartData.reverse();

	$window.AmCharts.makeChart( "daily-chart", {
      "type": "serial",
      "fontFamily": 'Roboto',
      "marginRight":30,
      "legend": {
					"enabled":false
      },
      "dataProvider": chartData,
      "valueAxes": [{
          "stackType": "regular",
          "gridAlpha": 0.07,
          "position": "left"
      }],
      "graphs": [{
        "balloonText": "<b>$[[total]]</b>",
        "fillAlphas": 0.8,
        "lineAlpha": 1,
        "lineThickness": 1,
        "lineColor": '#58cfd4',
        "visibleInLegend": false,
        "valueField": 'value'
      }],
      "plotAreaBorderAlpha": 0,
      "marginTop": 10,
      "marginLeft": 0,
      "marginBottom": 0,
      "chartCursor": {
          "cursorAlpha": 0.3,
					"cursorColor": '#000',
					"zoomable": false
      },
      "dataDateFormat": "YYYY-MM-DD",
      "categoryField": "day",
      "categoryAxis": {
          "dateFormats": [{
              "period": "DD",
              "format": "DD/MM"
          }, {
              "period": "WW",
              "format": "DD/MM"
          }, {
              "period": "MM",
              "format": "DD/MM"
          }, {
              "period": "YYYY",
              "format": "YYYY"
          }],
          "parseDates": true,
          "axisColor": "#555555",
          "gridAlpha": 0.1,
          "gridColor": "#FFFFFF",
      }
  });

	//console.log(chartData);
},console.log);


$scope.formattedDate = function(v){
	return moment(v).format('hh:mm MM/DD/YYYY');
};

}]);
