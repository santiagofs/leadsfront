angular.module('app.directives')
    .directive('listPagination', ['$filter',function($filter) {

        return {
            restrict: 'AE',
            replace: true,
            templateUrl: '../src/directives/pagination.tpl.html',
            transclude: true,
            //require: '^form',
            scope: {
                pagination: '=',
                onPage: '&'
            },
            link: function($scope, $elem, $attrs, form) {

                $scope.current = 1;
                $scope.pageList = [];
                $scope.$watch('pagination', function(newValue, oldValue){
                    //console.log(newValue);
                    if(!newValue || newValue === oldValue) return;
                    $scope.current = newValue.page;
                    $scope.pageList = [];
                    //console.log($scope.pagination);
                    for(var i = newValue.first; i<= newValue.last; i++) {
                        $scope.pageList.push({label:i, value:i});
                    }
                });


                $scope.goTo = function(page) {
                    $scope.onPage({page: page});
                }
                $scope.onPageChange = function() {
                    $scope.onPage({page: $scope.current});
                }
            }
        };
    }]);
