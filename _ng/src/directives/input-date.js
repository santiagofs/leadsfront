angular.module('app.directives')
    .directive('inputDate', ['$filter',function($filter) {

        return {
            restrict: 'AE',
            replace: true,
            templateUrl: '../src/directives/input-date.tpl.html',
            transclude: true,
            //require: '^form',
            scope: {
                date: '=',
                timeMode: '=',
                minuteInterval: '='
            },
            link: function($scope, $elem, $attrs, form) {
                $scope.required = $elem.attr('required') !== undefined;
                $scope.withTime = $elem.attr('with-time') !== undefined;
                $scope.form = form;

                if(!$scope.timeMode) $scope.timeMode = '12';
                if(!$scope.minuteInterval) $scope.minuteInterval = '15';


                if($scope.date === 'now') {
	                $scope.date = moment();
                }

				function parseDate() {
					var m = moment.isMoment($scope.date) ? $scope.date : moment($scope.date, 'YYYY-MM-DD HH:mm:ss');

					if (m == null || !m.isValid()) {
	                    //m = moment('1970-01-01', 'YYYY-MM-DD');
	                    $scope.parsed = {
		                    year: '',
		                    month: '',
		                    day: '',
		                    hour: '',
		                    minute: '',
		                    second: '',
		                    timezone: ''
		                };

					} else {
						$scope.parsed = {
							year: m.year(),
							month: m.month() + 1,
							day: m.date(),
							hour: m.hour(),
							minute: m.minute(),
							second: m.second(),
							timezone: m.utcOffset()
						};
					}
				}


                parseDate();

				var i;
				$scope.ranges = {};
                function getRanges() {


                    $scope.ranges.years = [];
                    var maxYear = $attrs.maxYear || moment().year();
                    var minYear = $attrs.minYear || 1900;
                    for (i = maxYear; i >= minYear; i--) {
                        $scope.ranges.years.push(i);
                    }

                    $scope.ranges.months = [];
                    for (i = 1; i <= 12; i++) {
                        $scope.ranges.months.push(i);
                    }

                    $scope.ranges.days = [];
                    var d = new Date($scope.parsed.year, $scope.parsed.month, 0);
                    var maxDays = d.getDate();

                    for (i = 1; i <= maxDays; i++) {
                        $scope.ranges.days.push(i);
                    }

                    if ($scope.parsed.day > maxDays) {
                        $scope.parsed.day = maxDays;
                    }


                }

                getRanges();
				if($scope.withTime) {
					$scope.ranges.hours = [];
					$scope.ranges.minutes = [];
					$scope.ranges.timezones = [];

					if($scope.timeMode.toString() === '12') {

						for (i = 1; i <= 24; i++) {
							var val = (i%12)
							var hour = {
								value: i === 24 ? 0 : i,
								display: $filter('numberFixedLen')((val===0? 12 : val), 2) + ' ' + (i<=12 ? 'AM' : 'PM')
							}
	                        $scope.ranges.hours.push(hour);
	                    }
					}

					for(i=0; i <60; i=i+$scope.minuteInterval) {
						 $scope.ranges.minutes.push(i);
					}
					for( i=-12; i<=12; i++) {
						var timezone = {
							value: i*60,
							display: i
						}
						$scope.ranges.timezones.push(timezone);
					}
				}

                function setDate() {

                    var year = $scope.parsed.year,
                        month = $scope.parsed.month-1,
                        day = $scope.parsed.day,
                        hour = $scope.parsed.hour,
                        minute = $scope.parsed.minute,
                        second = $scope.parsed.second,
                        timezone = $scope.parsed.timezone,
                        date = null;

					if($scope.withTime) {
						if (year !=='' && month !=='' && day !=='' && hour !=='' && minute !=='' && second !=='' && timezone !=='') {
							date = moment.utc([year, month, day, hour, minute, second]);
              date.utcOffset(-timezone);
              $scope.date = date.format('YYYY-MM-DD HH:mm:ss');
						}
					} else {
						if (year && month && day) {
	                        date = moment([year, month, day, 0,0,0]).utcOffset(0);
	                        $scope.date = date.format('YYYY-MM-DD');
	                    }
					}

                }

                $scope.$watch(function() {
                    return $scope.parsed;
                }, function(newValue, oldValue) {
                    if (newValue !== oldValue) {
                        getRanges();
                        setDate();
                    }
                }, true);
            }
        };
    }]);
