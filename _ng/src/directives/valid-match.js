angular.module('app.directives')
  .directive('drcValidMatch', function(){
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=drcValidMatch"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue === scope.otherModelValue;
            };
 
            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
  });
