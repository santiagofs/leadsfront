angular.module('app.filters')
	.filter('numberFixedLen', function () {
	    return function(a,b){
	        return(1e4+""+a).slice(-b);
	    };
	}).filter('momentFormat', function() {
		return function(d, f){
			var m = moment(d, 'YYYY-MM-DD HH:mm:ss');
			if (!f) f = 'MM/DD/YYYY hh:mma';
			var ret = m.format(f);
			return ret;
		};
	});
