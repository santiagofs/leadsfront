angular.module('app.models')
    .factory('LeadModel', [function(){

		function empty() {
			return {
				id: null,
				name: null,
				email: null,
				phone: null,
				campaign_id: null,
				user_id: null,
				type_id: null,
				status_id: null,
				source: null,
        source_id: null,
        timezone: -3
			}
		}

		return {
			empty: empty
		};

	}]);
