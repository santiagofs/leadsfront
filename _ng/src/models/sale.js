angular.module('app.models')
    .factory('SaleModel', [function(){
		
		function empty() {
			return {
				id: null,
				user_id: null,
				lead_id: null,
				stock: null,
				amount: null,
				price: null,
				notes: null,
			}
		}
		
		return {
			empty: empty
		};
		
	}]);