angular.module('app.models')
    .factory('TimelineEventModel', [function(){

		function empty() {
			return {
				id: null,
				user_id: null,
				lead_id: null,
				lead_status_id: '',
				notes: null,
			}
		}

		return {
			empty: empty
		};

	}]);
