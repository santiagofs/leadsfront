angular.module('app.models')
    .factory('ChatModel', [function(){
		
		function empty() {
			return {
				id: null,
				from_id: null,
				to_id: null,
				body: null,
				seen: null,
			}
		}
		
		return {
			empty: empty
		};
		
	}]);