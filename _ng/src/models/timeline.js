angular.module('app.models')
    .factory('TimelineModel', [function(){
		
		function empty() {
			return {
				id: null,
				lead_id: null,
				user_id: null,
				timelineable_id: null,
				timelineable_type: null,
				timeline_type: null,
				timeline_value: null,
				created_at: null,
			}
		}
		
		return {
			empty: empty
		};
		
	}]);