angular.module('app.models')
    .factory('NotificationModel', [function(){

		function empty() {
			return {
				id: null,
				subject: null,
				body: null,
        read: 0,
			}
		}

		return {
			empty: empty
		};

	}]);
