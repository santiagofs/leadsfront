angular.module('app.models')
    .factory('NoteModel', [function(){
		
		function empty() {
			return {
				id: null,
				lead_id: null,
				user_id: null,
				subject: null,
				body: null,
			}
		}
		
		return {
			empty: empty
		};
		
	}]);