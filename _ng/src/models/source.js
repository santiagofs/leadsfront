angular.module('app.models')
    .factory('SourceModel', [function(){

		function empty() {
			return {
				id: null,
				source: null,
			}
		}

		return {
			empty: empty
		};

	}]);
