angular.module('app.models')
    .factory('UserRoleModel', [function(){
		
		function empty() {
			return {
				id: null,
				role: null,
			}
		}
		
		return {
			empty: empty
		};
		
	}]);
