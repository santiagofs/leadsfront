angular.module('app.models')
    .factory('ScheduleModel', [function(){
		
		function empty() {
			return {
				id: null,
				user_id: null,
				lead_id: null,
				call_date: null,
				notes: null,
			}
		}
		
		return {
			empty: empty
		};
		
	}]);