angular.module('app.models')
    .factory('CampaignModel', [function(){

		function empty() {
			return {
				id: null,
				name: null,
				start_date: moment().format('YYYY-MM-DD'),
				end_date: moment().add(1, 'months').format('YYYY-MM-DD'),
        active: 0
			}
		}

		return {
			empty: empty
		};

	}]);
