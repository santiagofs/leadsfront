angular.module('app.models')
    .factory('LeadTypeModel', [function(){
		
		function empty() {
			return {
				id: null,
				name: null,
			}
		}
		
		return {
			empty: empty
		};
		
	}]);
