angular.module('app.models')
    .factory('LeadStatusModel', [function(){
		
		function empty() {
			return {
				id: null,
				name: null,
			}
		}
		
		return {
			empty: empty
		};
		
	}]);
