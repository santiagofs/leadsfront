angular.module('app.models')
    .factory('UserModel', [function(){

		function empty() {
			return {
				id: null,
				name: null,
				email: null,
				password: null,
				role_id: null,
				active: 1,
				api_token: null
			}
		}

		return {
			empty: empty
		};

	}]);
