var fs = require('fs'),
		glob = require('glob'),
		path = require('path'),
		gulp = require('gulp'),
		sourcemaps = require('gulp-sourcemaps'),
		sass = require('gulp-sass'),
		gutil = require('gulp-util'),
		concat = require('gulp-concat'),
		uglify = require('gulp-uglify'),
        html2js = require('gulp-html2js'),
		livereload = require('gulp-livereload'),
		jshint = require('gulp-jshint'),
		stylish = require('jshint-stylish');

var temp = './build/';
var assets = '../assets/';

gulp.task('sass-includes', function () {
	var all = 'all.scss';
	glob('./src/css/scss/**/' + all, function (error, files) {
		files.forEach(function (allFile) {
			// Add a banner to warn users
			fs.writeFileSync(allFile, '/** This is a dynamically generated file **/\n\n');

			var directory = path.dirname(allFile);
			var partials = fs.readdirSync(directory).filter(function (file) {
				return (
					// Exclude the dynamically generated file
					file !== all &&
					// Only include _*.scss files
					path.extname(file) === '.scss'
				);
			});

			// Append import statements for each partial
			partials.forEach(function (partial) {
				fs.appendFileSync(allFile, '@import "' + partial + '";\n');
			});
		});
	});
});

gulp.task('sass', ['sass-includes'], function(){
  return gulp.src('./src/css/scss/app.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(assets))
    .pipe(livereload());
});


gulp.task('js:vendor', function () {
	gulp.src([
		'node_modules/jquery/dist/jquery.js',
		'node_modules/moment/moment.js',
		'node_modules/bootstrap/dist/js/bootstrap.js',
		'node_modules/bootstrap-table/dist/bootstrap-table.js',
		'node_modules/bootstrap-table/dist/extensions/editable/bootstrap-table-editable.js',
		'node_modules/bootstrap-table/dist/extensions/flat-json/bootstrap-table-flat-json.js',

		'vendor/x-editable.bootstrap-editable.js',
		// 'js/vendor/jquery.blockUI.js',
		//'node_modules/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
		'vendor/bootstrap-datetimepicker.min.js',
		// 'js/vendor/select2.min.js',
		'node_modules/block-ui/jquery.blockUI.js',
		'node_modules/angular/angular.js',
		'node_modules/angular-xeditable/dist/js/xeditable.js',
		'node_modules/angular-cookies/angular-cookies.js',
		'node_modules/angular-local-storage/dist/angular-local-storage.js',
		'node_modules/angular-moment/angular-moment.js',
		'node_modules/angular-ui-router/release/angular-ui-router.js',
		'node_modules/amcharts3/amcharts/amcharts.js',
		'node_modules/amcharts3/amcharts/serial.js',
		'node_modules/xlsx/dist/jszip.js',
		'node_modules/xlsx/dist/xlsx.js',
		'node_modules/xlsx/dist/ods.js',
		// 'js/vendor/datetimepicker.js',
		// 'js/vendor/datetimepicker.templates.js',
	])
		.pipe(concat('vendor.js'))
		.pipe(uglify())
		.pipe(gulp.dest(assets))
		.on('error', gutil.log);
});

gulp.task('js:templates', function () {
    gulp.src('src/**/*.tpl.html')
    	.pipe(html2js('templates.js', {
            adapter: 'angular',
            base: 'templates',
            name: 'templates-app'
        }))
        .pipe(gulp.dest(temp))
        .on('error', gutil.log);

});

gulp.task('js:app', function () {
	gulp.src('src/**/*.js')
		//.pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter(stylish))
		.pipe(concat('app.js'))
		.pipe(gulp.dest(temp))
		.on('error', gutil.log);
});

gulp.task('js:all', ['js:app'], function () {
	setTimeout(function() {
        gulp.src([
            //temp + 'vendor.js',
            temp + 'templates.js',
            temp + 'app.js'
        ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest(assets))
		.pipe(livereload())
	}, 1000);
});

gulp.task('watch', function(){
	livereload.listen();
	gulp.watch('src/css/scss/**/!(all).scss', ['sass']);
	gulp.watch('src/**/*.tpl.html', ['js:templates', 'js:all']);
	gulp.watch('src/**/*.js', ['js:app', 'js:all']);
});

gulp.task('default', ['sass', 'js:app', 'watch']);
